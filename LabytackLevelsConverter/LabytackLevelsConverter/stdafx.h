// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#include <iostream>
#include <stdio.h>
#include <tchar.h>

using namespace std;

#define LEFT_OFFSET 12
#define RIGHT_OFFSET 4
#define TOP_OFFSET 8
#define BOTTOM_OFFSET 0

#define USUAL_MASK 1
#define BREAKABLE_MASK 2
#define DIODE_MASK 4
#define INVERSE_DIODE_MASK 8
#define CLEAR_MASK 0

#define USUAL_CELL 1
#define FINISH_CELL 2
#define CHECKED_FINISH_CELL 4

// TODO: reference additional headers your program requires here
