========================================================================
    CONSOLE APPLICATION : LabytackLevelsConverter Project Overview
========================================================================

AppWizard has created this LabytackLevelsConverter application for you.

This file contains a summary of what you will find in each of the files that
make up your LabytackLevelsConverter application.


LabytackLevelsConverter.vcproj
    This is the main project file for VC++ projects generated using an Application Wizard.
    It contains information about the version of Visual C++ that generated the file, and
    information about the platforms, configurations, and project features selected with the
    Application Wizard.

LabytackLevelsConverter.cpp
    This is the main application source file.

/////////////////////////////////////////////////////////////////////////////
Other standard files:

StdAfx.h, StdAfx.cpp
    These files are used to build a precompiled header (PCH) file
    named LabytackLevelsConverter.pch and a precompiled types file named StdAfx.obj.

/////////////////////////////////////////////////////////////////////////////
Other notes:

AppWizard uses "TODO:" comments to indicate parts of the source code you
should add to or customize.

/////////////////////////////////////////////////////////////////////////////
