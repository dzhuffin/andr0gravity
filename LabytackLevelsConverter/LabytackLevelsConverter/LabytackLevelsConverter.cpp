// LabytackLevelsConverter.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

void setCellType(int *data, int type)
{
	switch (type)
	{
		case 0:	
			*data  &= 0xFFF0FFFF;
			*data  |= (USUAL_CELL << 16);
			break;
		case 1:
			*data  &= 0xFFF0FFFF;
			*data  |= (FINISH_CELL << 16);
			break;
		case 2:
			*data  &= 0xFFF0FFFF;
			*data  |= (CHECKED_FINISH_CELL << 16);
			break;
		default:
			break;
	}	
}

void setWallKind(int *data, int dir, int kind)
{
	int k;
	switch (kind)
	{
		case 0:			// clear
			k = CLEAR_MASK;
			break;
		case 1:			// usual
			k = USUAL_MASK;
			break;
		case 2:			// breakable
			k = BREAKABLE_MASK;
			break;
		case 3:			// diode
			k = DIODE_MASK;
			break;
		case 4:			// inverse diode
			k = INVERSE_DIODE_MASK;
			break;
		default:
			return;
			break;
	}

	switch (dir)
	{
		case 0:		// left
			*data &= ~(0xF << LEFT_OFFSET);
			*data |= (k << LEFT_OFFSET);
			break;
		case 1:		// right
			*data &= ~(0xF << RIGHT_OFFSET);
			*data |= (k << RIGHT_OFFSET);
			break;
		case 2:		// top
			*data &= ~(0xF << TOP_OFFSET);
			*data |= (k << TOP_OFFSET);
			break;
		case 3:		// bottom
			*data &= ~(0xF << BOTTOM_OFFSET);
			*data |= (k << BOTTOM_OFFSET);
			break;
		default:
			return;
			break;
	}	
}

int _tmain(int argc, _TCHAR* argv[])
{
	if (argc != 3)
	{
		_tprintf(_T("Incorrect arguments!\nLabytackLevelsConverter.exe srcFileName(*.txt) destFileName(*.lab)\nPress any key..."));
		getchar();
		return 0;
	}

	int i = 0, j = 0;
/////////////load values///////////////
	int width = 0;
	int height = 0;
	int startX = 0;
	int startY = 0;
	int fininshX = 0;
	int fininshY = 0;
	int finCount = 0;
	int *finX = NULL, *finY = NULL;
	int **data = NULL;
	int wall = 0;
//\\\\\\\\\\\load values\\\\\\\\\\\\\\\

	FILE *inFile;
	//char *in_file_name = "i:\\exchange\\Android\\Projects\\Labytack\\LabytackLevelsConverter\\Debug\\lev_blank.txt";
	_TCHAR *in_file_name = argv[1];

	inFile = _tfopen(in_file_name, _T("r"));
	if (inFile != 0)
	{
		fscanf(inFile, "%d %d\n\n", &width, &height);
		fscanf(inFile, "%d %d\n\n", &startX, &startY);		
		fscanf(inFile, "%d %d\n\n", &fininshX, &fininshY);

		_tprintf(_T("w=%d, h=%d\n"), width, height);
		_tprintf(_T("stX=%d, stY=%d\n"), startX, startY);
		_tprintf(_T("fnX=%d, fnY=%d\n"), fininshX, fininshY);

		data = new int*[width];
		for (i = 0; i < width; i++)
			data[i] = new int[height];			

		for (i = 0; i < width; i++)
			for (j = 0; j < height; j++)
				data[i][j] = 0;

		fscanf(inFile, "%d\n", &finCount);
		_tprintf(_T("finCount=%d\n"), finCount);
		if (finCount > 0)
		{
			finX = new int[finCount];
			finY = new int[finCount];
			for (i = 0; i < finCount; i++)
			{
				fscanf(inFile, "%d %d\n", &finX[i], &finY[i]);
				_tprintf(_T("finX[%d]=%d, finY[%d]=%d\n"), i, finX[i], i, finY[i]);
			}
		}

		fscanf(inFile, "\n");

		for (i = 0; i < width; i++)
		{
			for (j = 0; j < width + 1; j++)
			{
				fscanf(inFile, "%d ", &wall);

				if (j == 0)
					setWallKind(&data[i][j], 0, wall);
				else
				{
					if (j == width)
						setWallKind(&data[i][j - 1], 1, wall);
					else
					{
						if (wall == 3)
						{
							setWallKind(&data[i][j - 1], 1, wall);
							setWallKind(&data[i][j], 0, wall + 1);
						}
						else
						{
							if (wall == 4)
							{
								setWallKind(&data[i][j - 1], 1, wall);
								setWallKind(&data[i][j], 0, wall - 1);
							}
							else
							{
								setWallKind(&data[i][j - 1], 1, wall);
								setWallKind(&data[i][j], 0, wall);
							}
						}
					}
				}				
			}	
			fscanf(inFile, "\n");
		}

		fscanf(inFile, "\n");

		for (i = 0; i < width + 1; i++)
		{
			for (j = 0; j < width; j++)
			{
				fscanf(inFile, "%d ", &wall);

				setCellType(&data[fininshX][fininshY], 0);

				if (i == 0)
					setWallKind(&data[i][j], 2, wall);
				else
				{
					if (i == width)
						setWallKind(&data[i - 1][j], 3, wall);
					else
					{
						if (wall == 3)
						{
							setWallKind(&data[i - 1][j], 3, wall);
							setWallKind(&data[i][j], 2, wall + 1);
						}
						else
						{
							if (wall == 4)
							{
								setWallKind(&data[i - 1][j], 3, wall);
								setWallKind(&data[i][j], 2, wall - 1);
							}
							else
							{
								setWallKind(&data[i - 1][j], 3, wall);
								setWallKind(&data[i][j], 2, wall);
							}
						}
					}
				}				
			}	
			fscanf(inFile, "\n");
		}

		for (int i = 0; i < finCount; i++)
			setCellType(&data[finX[i]][finY[i]], 2);

		setCellType(&data[fininshX][fininshY], 1);
	}
	else
	{
		_tprintf(_T("File no found! Press any key...\n")); 
		getchar();

		return 0;
	}

	fclose(inFile);

	/////////////////writing//level//////////////////////
	FILE *outFile;
	//char *out_file_name = "i:\\exchange\\Android\\Projects\\Labytack\\LabytackLevelsConverter\\Debug\\lev_blank.lab";
	_TCHAR *out_file_name = argv[2];

	outFile = _tfopen(out_file_name, _T("w+b"));
	if (outFile != 0)
	{
		fwrite(&width, sizeof(unsigned char), 1, outFile);
		fwrite(&height, sizeof(unsigned char), 1, outFile);
		fwrite(&startX, sizeof(unsigned char), 1, outFile);
		fwrite(&startY, sizeof(unsigned char), 1, outFile);
		fwrite(&fininshX, sizeof(unsigned char), 1, outFile);
		fwrite(&fininshY, sizeof(unsigned char), 1, outFile);

		for (i = 0; i < width; i++)
			for (j = 0; j < height; j++)
				fwrite(&data[i][j], sizeof(int), 1, outFile);

		fclose(outFile);
	}
	else
	{
		_tprintf(_T("Cann't save level! Press any key...\n")); 
		getchar();

		return 0;
	}

	_tprintf(_T("Converted successful! Press any key...\n")); 
	getchar();

	return 0;
}

