package com.spiritgames.andr0gravity;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
 
public class SensorReader implements SensorEventListener
{
	private SensorManager sensorManager;
	private float[] orientation = {0.0f, 0.0f, 0.0f};
	
	public float getXOrientation()
	{
		return this.orientation[0];
	}
	
	public float getYOrientation()
	{
		return this.orientation[1];
	}
	
	public float getZOrientation()
	{
		return this.orientation[2];
	}
	
	public SensorReader(Context context)
	{
		this.sensorManager = (SensorManager)context.getSystemService(Context.SENSOR_SERVICE);
	}
	
	public void register()
	{
		this.sensorManager.registerListener(this, this.sensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION), SensorManager.SENSOR_DELAY_GAME);
	}
	
	public void unRegister()
	{
		this.sensorManager.unregisterListener(this);
	}	

	public void onSensorChanged(SensorEvent event) 
	{
		if (event.sensor.getType() == Sensor.TYPE_ORIENTATION)  
        { 
             this.orientation = event.values;               
        } 
	}
	
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
		// TODO Auto-generated method stub
		
	}
}
