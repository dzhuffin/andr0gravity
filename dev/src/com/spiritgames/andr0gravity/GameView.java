package com.spiritgames.andr0gravity;

import android.content.Context;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class GameView extends SurfaceView implements SurfaceHolder.Callback 
{
	public GameRenderer mRenderer;
	
	private SurfaceHolder mSurfaceHolder;
	private Context context;
	
	public GameView(Context context, AttributeSet attrs)
	{		
		super(context, attrs);

		this.context = context;
        this.mSurfaceHolder = this.getHolder();
        this.mSurfaceHolder.addCallback(this); 
        
        this.mRenderer = new GameRenderer(this.mSurfaceHolder, this.context);
	}
	
	public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) 
	{		
		this.mRenderer.initBackBuffer(width, height);
	}

	public void surfaceCreated(SurfaceHolder holder) 
	{
		this.mRenderer.setRunning(true);
		this.mRenderer.setPriority(Thread.MAX_PRIORITY);
		if (!this.mRenderer.isAlive())
			this.mRenderer.start();
	}

	public void surfaceDestroyed(SurfaceHolder holder) 
	{
	}
}
