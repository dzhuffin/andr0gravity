package com.spiritgames.andr0gravity;

import com.smaato.SOMA.SOMABanner;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.Paint.Style;
import android.graphics.drawable.BitmapDrawable;

public class MMManager
{
	class StartLogoThread extends Thread
	{
		@Override 
		public void run()
		{		
			try 
			{
				Thread.sleep(3000);
			} 
			catch (InterruptedException e) 
			{			
				e.printStackTrace();
			}
			finally
			{				
				MMManager.this.setCurrentLayer(MMManager.LAYER_GAME_RATING);
			}
			
			try 
			{
				Thread.sleep(4000);
			} 
			catch (InterruptedException e) 
			{			
				e.printStackTrace();
			}
			finally
			{				
				MMManager.this.setCurrentLayer(MMManager.LAYER_GAME_LOGO);
			}
			
			try 
			{
				Thread.sleep(3000);
			} 
			catch (InterruptedException e) 
			{			
				e.printStackTrace();
			}
			finally
			{				
				MMManager.this.setCurrentLayer(MMManager.LAYER_MAIN);
			}
		}
	}
	
	private StartLogoThread mStartLogoThread;
	
	public final static int LAYER_MAIN = 1;
	public final static int LAYER_LEVELS = 2;
	public final static int LAYER_OPTIONS = 3;
	public final static int LAYER_HISTORY = 4;
	public final static int LAYER_INSTRUCTIONS = 5;
	public final static int LAYER_GAME_LOGO = 6;
	public final static int LAYER_THE_END = 7;
	public final static int LAYER_ABOUT = 8;
	public final static int LAYER_KEYS = 9;
	public final static int LAYER_WALLS = 10;
	public final static int LAYER_CONTROLS = 11;
	public final static int LAYER_CONTEXT = 12;
	public final static int LAYER_SPIRIT_GAMES = 13;
	public final static int LAYER_GAME_RATING = 14;
	
	private int currentLayer = MMManager.LAYER_SPIRIT_GAMES;
	
	public final static int ITEM_START = 1;
	public final static int ITEM_SETTINGS = 2;
	public final static int ITEM_EXIT = 3;
	public final static int ITEM_ABOUT = 4;
	
	public final static int ITEM_LEVEL_MINUS = 5;
	public final static int ITEM_LEVEL_PLUS = 6;
	public final static int ITEM_LEVEL = 7;
	
	public final static int ITEM_OPTIONS_ANIMATION = 8;
	public final static int ITEM_OPTIONS_SOUND = 9;
	public final static int ITEM_OPTIONS_SENSOR = 10;
	
	public final static int ITEM_CONTEXT_RESTART = 11;
	public final static int ITEM_CONTEXT_CANCEL = 12;
	
	private int pressedItem = 0;
	
	private BitmapDrawable mLogo;
	private BitmapDrawable mGameLogo;
	private BitmapDrawable mGameRating;
	private BitmapDrawable mTheEnd;
	
    private BitmapDrawable mStartItem;
    private BitmapDrawable mSettingsItem;
    private BitmapDrawable mExitItem;
    private BitmapDrawable mAboutItem;    
    private BitmapDrawable mStartItemPressed;
    private BitmapDrawable mSettingsItemPressed;
    private BitmapDrawable mExitItemPressed;
    private BitmapDrawable mAboutItemPressed;
    
    private BitmapDrawable mLevelMinusItem;
    private BitmapDrawable mLevelPlusItem;
    private BitmapDrawable mLevelItem;
    private BitmapDrawable mLevelMinusItemPressed;
    private BitmapDrawable mLevelPlusItemPressed;
    
    private BitmapDrawable mOptionsAnimationItem;
    private BitmapDrawable mOptionsSoundItem;
    private BitmapDrawable mOptionsSensorItem;
    private BitmapDrawable mOptionsAnimationItemChecked;
    private BitmapDrawable mOptionsSoundItemChecked;
    private BitmapDrawable mOptionsSensorItemChecked;
    
    private BitmapDrawable mContextRestartItem;
    private BitmapDrawable mContextRestartItemPressed;
    private BitmapDrawable mContextCancelItem;
    private BitmapDrawable mContextCancelItemPressed;
    
    private BitmapDrawable mHistoryItem;
    private BitmapDrawable mKeysTextItem;
    private BitmapDrawable mWallsTextItem;
    private BitmapDrawable mControlsTextItem;
    
    private BitmapDrawable mAboutTextItem;
    
    private Resources res;
    // animation
    private boolean animationEnabled = true;
    public boolean getAnimationEnabled()
    {
    	return this.animationEnabled;
    }
    public void setAnimationEnabled(boolean val)
    {
    	this.animationEnabled = val;
    }
    // sound
    private boolean soundEnabled = true;
    public boolean getSoundEnabled()
    {
    	return this.soundEnabled;
    }
    public void setSoundEnabled(boolean val)
    {
    	this.soundEnabled = val;
    }
    // sensor
    private boolean sensorEnabled = true;
    public boolean getSensorEnabled()
    {
    	return this.sensorEnabled;
    }
    public void setSensorEnabled(boolean val)
    {
    	this.sensorEnabled = val;
    }
    //
    private Paint mLevelsCaptionFont;
    private Paint mLevelsFont;
    
    private int selectedLevel;
    private int reachedLevel;
    public void setReachedLevel(int val)
    {
    	this.selectedLevel = val;
    	this.reachedLevel = val;
    }
    public int getSelectedLevel()
    {    
    	return this.selectedLevel;
    }
    public void IncSelectedLevel()
    {
    	if (this.selectedLevel < this.reachedLevel)
    		this.selectedLevel++;
    }
    public void DecSelectedLevel()
    {
    	if (this.selectedLevel > 1)
    		this.selectedLevel--;
    }
    
    public MMManager(Context con)
    {
    	System.gc();
    	
    	this.res = con.getResources();
    	
    	this.mLogo = (BitmapDrawable)this.res.getDrawable(R.drawable.logo);
    	this.mGameLogo = (BitmapDrawable)this.res.getDrawable(R.drawable.game_logo);
    	this.mGameRating = (BitmapDrawable)this.res.getDrawable(R.drawable.rating);
    	this.mTheEnd = (BitmapDrawable)this.res.getDrawable(R.drawable.the_end);
    	
    	this.mStartItem = (BitmapDrawable)this.res.getDrawable(R.drawable.mm_start);
        this.mSettingsItem = (BitmapDrawable)this.res.getDrawable(R.drawable.mm_settings);
        this.mExitItem = (BitmapDrawable)this.res.getDrawable(R.drawable.mm_exit);
        this.mAboutItem = (BitmapDrawable)this.res.getDrawable(R.drawable.mm_about);
        this.mStartItemPressed = (BitmapDrawable)this.res.getDrawable(R.drawable.mm_start_press);
        this.mSettingsItemPressed = (BitmapDrawable)this.res.getDrawable(R.drawable.mm_settings_press);
        this.mExitItemPressed = (BitmapDrawable)this.res.getDrawable(R.drawable.mm_exit_press);        
        this.mAboutItemPressed = (BitmapDrawable)this.res.getDrawable(R.drawable.mm_about_press);
        
        this.mLevelMinusItem = (BitmapDrawable)this.res.getDrawable(R.drawable.mm_level_minus);
        this.mLevelPlusItem = (BitmapDrawable)this.res.getDrawable(R.drawable.mm_level_plus);
        this.mLevelItem = (BitmapDrawable)this.res.getDrawable(R.drawable.mm_level);
        this.mLevelMinusItemPressed = (BitmapDrawable)this.res.getDrawable(R.drawable.mm_level_minus_press);
        this.mLevelPlusItemPressed = (BitmapDrawable)this.res.getDrawable(R.drawable.mm_level_plus_press);
        
        this.mOptionsAnimationItem = (BitmapDrawable)this.res.getDrawable(R.drawable.mm_ani_enable);
        this.mOptionsSoundItem = (BitmapDrawable)this.res.getDrawable(R.drawable.mm_snd_enable);
        this.mOptionsSensorItem = (BitmapDrawable)this.res.getDrawable(R.drawable.mm_sensor_enable);
        this.mOptionsAnimationItemChecked = (BitmapDrawable)this.res.getDrawable(R.drawable.mm_ani_disable);
        this.mOptionsSoundItemChecked = (BitmapDrawable)this.res.getDrawable(R.drawable.mm_snd_disable);
        this.mOptionsSensorItemChecked = (BitmapDrawable)this.res.getDrawable(R.drawable.mm_sensor_disable);
        
        this.mContextRestartItem = (BitmapDrawable)this.res.getDrawable(R.drawable.mm_restart);
        this.mContextRestartItemPressed = (BitmapDrawable)this.res.getDrawable(R.drawable.mm_restart_press);
        this.mContextCancelItem = (BitmapDrawable)this.res.getDrawable(R.drawable.mm_cancel);
        this.mContextCancelItemPressed = (BitmapDrawable)this.res.getDrawable(R.drawable.mm_cancel_press);
        
        this.mHistoryItem = (BitmapDrawable)this.res.getDrawable(R.drawable.mm_history);
        this.mKeysTextItem = (BitmapDrawable)this.res.getDrawable(R.drawable.keys_text);
        this.mWallsTextItem = (BitmapDrawable)this.res.getDrawable(R.drawable.walls_text);
        this.mControlsTextItem = (BitmapDrawable)this.res.getDrawable(R.drawable.controls_text);
        
        this.mAboutTextItem = (BitmapDrawable)this.res.getDrawable(R.drawable.mm_about_text);
        
    	this.mLevelsFont = new Paint();    	
    	this.mLevelsFont.setStrokeWidth(1);
    	this.mLevelsFont.setStyle(Style.FILL);
    	this.mLevelsFont.setColor(Color.GREEN);
    	this.mLevelsFont.setTextAlign(Paint.Align.CENTER);
    	this.mLevelsFont.setTypeface(Typeface.createFromAsset(con.getAssets(), "fonts/digiface.ttf"));
    	
    	this.mLevelsCaptionFont = new Paint();    	
    	this.mLevelsCaptionFont.setStrokeWidth(1);
    	this.mLevelsCaptionFont.setStyle(Style.FILL);
    	this.mLevelsCaptionFont.setColor(Color.GREEN);
    	this.mLevelsCaptionFont.setTextAlign(Paint.Align.CENTER);
    	this.mLevelsCaptionFont.setTypeface(Typeface.createFromAsset(con.getAssets(), "fonts/cristal.ttf"));
    	
    	this.mStartLogoThread = new StartLogoThread();
    	this.mStartLogoThread.start();
    }
    
    public Rect getItemBounds(int item)
    {    	
    	switch (item)
    	{
    		case MMManager.ITEM_START:
    			return this.mStartItem.getBounds();
    		case MMManager.ITEM_SETTINGS:    			
    			return this.mSettingsItem.getBounds();
    		case MMManager.ITEM_EXIT:
    			return this.mExitItem.getBounds();
    		case MMManager.ITEM_ABOUT:
    			return this.mAboutItem.getBounds();
    		case MMManager.ITEM_LEVEL_MINUS:
    			return this.mLevelMinusItem.getBounds();
    		case MMManager.ITEM_LEVEL_PLUS:
    			return this.mLevelPlusItem.getBounds();
    		case MMManager.ITEM_LEVEL:
    			return this.mLevelItem.getBounds();
    		case MMManager.ITEM_OPTIONS_ANIMATION:
    			if (this.mOptionsAnimationItem.getBounds().width() < 1)
    				return this.mOptionsAnimationItemChecked.getBounds();
    			else
    				return this.mOptionsAnimationItem.getBounds();
    		case MMManager.ITEM_OPTIONS_SOUND:
    			if (this.mOptionsSoundItem.getBounds().width() < 1)
    				return this.mOptionsSoundItemChecked.getBounds();
    			else
    				return this.mOptionsSoundItem.getBounds();
    		case MMManager.ITEM_OPTIONS_SENSOR:
    			if (this.mOptionsSensorItem.getBounds().width() < 1)
    				return this.mOptionsSensorItemChecked.getBounds();
    			else
    				return this.mOptionsSensorItem.getBounds();
    		case MMManager.ITEM_CONTEXT_RESTART:
    			return this.mContextRestartItem.getBounds();
    		case MMManager.ITEM_CONTEXT_CANCEL:    			
    			return this.mContextCancelItem.getBounds();
    		default:
    			return this.mStartItem.getBounds();		
    	}    	 
    }
    
    public int getPressedItem()
    {
    	return this.pressedItem; 
    }
    public void setPressedItem(int item)
    {
    	this.pressedItem = item; 
    }
    
    public int getCurrentLayer()
    {
    	return this.currentLayer; 
    }
    public void setCurrentLayer(int val)
    {
    	this.currentLayer = val; 
    }
    
    public void Draw(Canvas backCanvas, int screenW, int screenH, SOMABanner banner)
    {
    	int startW;
    	if (screenW < screenH)
    	{
    		if (screenH > 400)
    			startW = (int)((float)screenW * 0.85f);
    		else
    			startW = (int)((float)screenW * 0.75f);
    	}
    	else
    	{
    		startW = screenW / 2;
    	}
    	float zoom = (float)startW / (float)this.mStartItem.getBitmap().getWidth();
    	
    	int aboutW = (int)((float)this.mAboutItem.getBitmap().getWidth() * zoom);
        int aboutH = (int)((float)this.mAboutItem.getBitmap().getHeight() * zoom);
        int aboutL = (screenW) - (aboutW) - 10;
    	int aboutT = (screenH) - (aboutH) - 10;
    	
    	int exitW = (int)((float)this.mExitItem.getBitmap().getWidth() * zoom);
        int exitH = (int)((float)this.mExitItem.getBitmap().getHeight() * zoom);
        int exitL;
        int exitT;
    	if (screenW < screenH)
    	{
    		exitL = (screenW / 2) - (exitW / 2);
    		exitT = aboutT - exitH;
    	}
        else
        {
        	exitL = 20;
        	exitT = aboutT;
        }
    	
    	int setW = (int)((float)this.mSettingsItem.getBitmap().getWidth() * zoom);
        int setH = (int)((float)this.mSettingsItem.getBitmap().getHeight() * zoom);
        int setL = (screenW / 2) - (setW / 2);
    	int setT = exitT - setH - 20;    	
    	
        int startH = (int)((float)this.mStartItem.getBitmap().getHeight() * zoom);
        int startL;
        int startT = setT - startH - 20;
        if (screenW < screenH)
        	startL = (screenW / 2) - (startW / 2);
        else
        	startL = 20;                                    	    
        
        int l_mW = (int)((float)this.mLevelMinusItem.getBitmap().getWidth() * zoom);
        int l_mH = (int)((float)this.mLevelMinusItem.getBitmap().getHeight() * zoom);
        int l_pW = (int)((float)this.mLevelPlusItem.getBitmap().getWidth() * zoom);
        int l_pH = (int)((float)this.mLevelPlusItem.getBitmap().getHeight() * zoom);
        int lW = (int)((float)this.mLevelItem.getBitmap().getWidth() * zoom);
        int lH = (int)((float)this.mLevelItem.getBitmap().getHeight() * zoom);
        
        int aniW = (int)((float)this.mOptionsAnimationItem.getBitmap().getWidth() * zoom);
        int aniH = (int)((float)this.mOptionsAnimationItem.getBitmap().getHeight() * zoom);
        int aniL = startL;
    	int aniT = startT;
        
        int sndW = (int)((float)this.mOptionsSoundItem.getBitmap().getWidth() * zoom);
        int sndH = (int)((float)this.mOptionsSoundItem.getBitmap().getHeight() * zoom);
        int sndL = setL;
    	int sndT = setT;
    	
    	int ratW = screenW;
        int ratH = screenH;
        int ratL = (screenW / 2) - (ratW / 2);
    	int ratT = (screenH / 2) - (ratH / 2);
    	
    	int senW = (int)((float)this.mOptionsSensorItem.getBitmap().getWidth() * zoom);
        int senH = (int)((float)this.mOptionsSensorItem.getBitmap().getHeight() * zoom);
        int senL = exitL;
    	int senT = exitT;
    	
    	int hisW = (int)((float)this.mHistoryItem.getBitmap().getWidth() * zoom);
        int hisH = (int)((float)this.mHistoryItem.getBitmap().getHeight() * zoom);
        int hisL = (screenW / 2) - (hisW / 2);
    	int hisT = (screenH / 2) - (hisH / 2);
    	
    	int sgW = (int)((float)this.mLogo.getBitmap().getWidth() * zoom);
        int sgH = (int)((float)this.mLogo.getBitmap().getHeight() * zoom);
        int sgL = (screenW / 2) - (sgW / 2);
    	int sgT = (screenH / 2) - (sgH / 2);
    	
    	int abTextW = (int)((float)this.mAboutTextItem.getBitmap().getWidth() * zoom);
        int abTextH = (int)((float)this.mAboutTextItem.getBitmap().getHeight() * zoom);
        int abTextL = (screenW / 2) - (abTextW / 2);
    	int abTextT = (screenH / 2) - (abTextH / 2);
    	
    	banner.setVisibility((this.currentLayer == MMManager.LAYER_GAME_RATING) ? 0 : 1);
    	
        switch (this.currentLayer)
        {
        	case MMManager.LAYER_MAIN:    	    	
    	    	if (this.pressedItem == MMManager.ITEM_START)
    	    	{
    	    		this.mStartItemPressed.setBounds(startL, startT, startL + startW, startT + startH);
    	    		this.mStartItemPressed.draw(backCanvas);
    	    	}
    	    	else
    	    	{
    	    		this.mStartItem.setBounds(startL, startT, startL + startW, startT + startH);
    	    		this.mStartItem.draw(backCanvas);
    	    	}
    	    	
    	    	if (this.pressedItem == MMManager.ITEM_SETTINGS)
    	    	{
    	    		this.mSettingsItemPressed.setBounds(setL, setT, setL + setW, setT + setH);
    	    		this.mSettingsItemPressed.draw(backCanvas);
    	    	}
    	    	else
    	    	{
    	    		this.mSettingsItem.setBounds(setL, setT, setL + setW, setT + setH);
    	    		this.mSettingsItem.draw(backCanvas);
    	    	}
    	    	
    	    	if (this.pressedItem == MMManager.ITEM_EXIT)
    	    	{
    	    		this.mExitItemPressed.setBounds(exitL, exitT, exitL + exitW, exitT + exitH);
    	    		this.mExitItemPressed.draw(backCanvas);
    	    	}
    	    	else
    	    	{
    	    		this.mExitItem.setBounds(exitL, exitT, exitL + exitW, exitT + exitH);
    	    		this.mExitItem.draw(backCanvas);
    	    	}
    	    	
    	    	if (this.pressedItem == MMManager.ITEM_ABOUT)
    	    	{
    	    		this.mAboutItemPressed.setBounds(aboutL, aboutT, aboutL + aboutW, aboutT + aboutH);
    	    		this.mAboutItemPressed.draw(backCanvas);
    	    	}
    	    	else
    	    	{
    	    		this.mAboutItem.setBounds(aboutL, aboutT, aboutL + aboutW, aboutT + aboutH);
    	    		this.mAboutItem.draw(backCanvas);
    	    	}
    	    	
        		break;
        	case MMManager.LAYER_LEVELS:
        		if (this.pressedItem == MMManager.ITEM_LEVEL_MINUS)
        		{
        			this.mLevelMinusItemPressed.setBounds(setL, setT, setL + l_mW, setT + l_mH);
	    			this.mLevelMinusItemPressed.draw(backCanvas);
        		}
        		else
        		{
        			this.mLevelMinusItem.setBounds(setL, setT, setL + l_mW, setT + l_mH);
	    			this.mLevelMinusItem.draw(backCanvas);
        		}
        		
        		if (this.pressedItem == MMManager.ITEM_LEVEL_PLUS)
        		{
        			this.mLevelPlusItemPressed.setBounds(setL + setW - l_pW, setT, setL + setW, setT + l_pH);
        			this.mLevelPlusItemPressed.draw(backCanvas);
        		}
        		else
        		{
        			this.mLevelPlusItem.setBounds(setL + setW - l_pW, setT, setL + setW, setT + l_pH);
        			this.mLevelPlusItem.draw(backCanvas);
        		}
        		
        		this.mLevelItem.setBounds((screenW / 2) - (lW / 2), setT, (screenW / 2) + (lW / 2), setT + lH);
	    		this.mLevelItem.draw(backCanvas);

	    		this.mLevelsCaptionFont.setTextSize(40);
	    			    		
        		backCanvas.drawText(this.res.getString(R.string.chooseLevel), (screenW / 2), setT - (setH * 2 / 3), this.mLevelsCaptionFont);
        		
        		if (this.pressedItem == MMManager.ITEM_LEVEL)
        			this.mLevelsFont.setColor(Color.WHITE);
        		else
        			this.mLevelsFont.setColor(Color.GREEN);
        		
        		this.mLevelsFont.setTextSize(setH * 2 / 3);
        		backCanvas.drawText(Integer.toString(this.selectedLevel), (screenW / 2), setT + (setH * 2 / 3), this.mLevelsFont);
        		break;
        	case MMManager.LAYER_OPTIONS:
        		if (this.animationEnabled)
        		{
        			this.mOptionsAnimationItem.setBounds(aniL, aniT, aniL + aniW, aniT + aniH);
	    			this.mOptionsAnimationItem.draw(backCanvas);
        		}
        		else
            	{
            		this.mOptionsAnimationItemChecked.setBounds(aniL, aniT, aniL + aniW, aniT + aniH);
    	    		this.mOptionsAnimationItemChecked.draw(backCanvas);
            	}        		
        			
        		if (this.soundEnabled)
        		{
        			this.mOptionsSoundItem.setBounds(sndL, sndT, sndL + sndW, sndT + sndH);
	    			this.mOptionsSoundItem.draw(backCanvas);
        		}
        		else
            	{
            		this.mOptionsSoundItemChecked.setBounds(sndL, sndT, sndL + sndW, sndT + sndH);
    	    		this.mOptionsSoundItemChecked.draw(backCanvas);
            	}
        		
        		if (this.sensorEnabled)
        		{
        			this.mOptionsSensorItem.setBounds(senL, senT, senL + senW, senT + senH);
	    			this.mOptionsSensorItem.draw(backCanvas);
        		}
        		else
            	{
            		this.mOptionsSensorItemChecked.setBounds(senL, senT, senL + senW, senT + senH);
    	    		this.mOptionsSensorItemChecked.draw(backCanvas);
            	}
        		break;
        	case MMManager.LAYER_CONTEXT:
        		if (this.pressedItem == MMManager.ITEM_CONTEXT_RESTART)
    	    	{
    	    		this.mContextRestartItemPressed.setBounds(setL, setT, setL + setW, setT + setH);
    	    		this.mContextRestartItemPressed.draw(backCanvas);
    	    	}
    	    	else
    	    	{
    	    		this.mContextRestartItem.setBounds(setL, setT, setL + setW, setT + setH);
    	    		this.mContextRestartItem.draw(backCanvas);
    	    	}
        		
        		if (this.pressedItem == MMManager.ITEM_CONTEXT_CANCEL)
    	    	{
    	    		this.mContextCancelItemPressed.setBounds(exitL, exitT, exitL + exitW, exitT + exitH);
    	    		this.mContextCancelItemPressed.draw(backCanvas);
    	    	}
    	    	else
    	    	{
    	    		this.mContextCancelItem.setBounds(exitL, exitT, exitL + exitW, exitT + exitH);
    	    		this.mContextCancelItem.draw(backCanvas);
    	    	}
        		break;
        	case MMManager.LAYER_HISTORY:
        		this.mHistoryItem.setBounds(hisL, hisT, hisL + hisW, hisT + hisH);
    			this.mHistoryItem.draw(backCanvas);
        		break;
        	case MMManager.LAYER_KEYS:
        		this.mKeysTextItem.setBounds(hisL, hisT, hisL + hisW, hisT + hisH);
    			this.mKeysTextItem.draw(backCanvas);
        		break;
        	case MMManager.LAYER_WALLS:
        		this.mWallsTextItem.setBounds(hisL, hisT, hisL + hisW, hisT + hisH);
    			this.mWallsTextItem.draw(backCanvas);
        		break;
        	case MMManager.LAYER_CONTROLS:
        		this.mControlsTextItem.setBounds(hisL, hisT, hisL + hisW, hisT + hisH);
    			this.mControlsTextItem.draw(backCanvas);
        		break;
        	case MMManager.LAYER_ABOUT:
        		this.mAboutTextItem.setBounds(abTextL, abTextT, abTextL + abTextW, abTextT + abTextH);
    			this.mAboutTextItem.draw(backCanvas);
        		break;
        	case MMManager.LAYER_GAME_LOGO:
        		this.mGameLogo.setBounds(hisL, hisT, hisL + hisW, hisT + hisH);
    			this.mGameLogo.draw(backCanvas);
        		break;
        	case MMManager.LAYER_GAME_RATING:
        		this.mGameRating.setBounds(ratL, ratT, ratL + ratW, ratT + ratH);
    			this.mGameRating.draw(backCanvas);
        		break;
        	case MMManager.LAYER_SPIRIT_GAMES:
        		this.mLogo.setBounds(sgL, sgT, sgL + sgW, sgT + sgH);
    			this.mLogo.draw(backCanvas);
        		break;
        	case MMManager.LAYER_THE_END:
        		this.mTheEnd.setBounds(hisL, hisT, hisL + hisW, hisT + hisH);
    			this.mTheEnd.draw(backCanvas);
        		break;
        	default:
        		break;
        }
    }
}
