package com.spiritgames.andr0gravity;

import java.util.HashMap;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Paint.Style;
import android.graphics.drawable.BitmapDrawable;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.SystemClock;

public class Labyrinth 
{
	private LevelsIOProvider mLevelsIOProvider;
	private Resources res;
	private String levelsPath = "";
	public void setLevelsPath(String val)
	{
		this.levelsPath = val;
	}	
	
	private int offset = 10; 
	private int width;
	public int getWidth()
	{
		return this.width;
	}
	private int height;
	public int getHeight()
	{
		return this.height;
	}
	public void setLabyrinthWH(int w, int h)
	{
		this.width = w;
		this.height = h;
		this.data = new int[w][h];
	}
	private Point startPos;
	public Point getStartPos()
	{
		return this.startPos;
	}
	public void setStartPos(Point val)
	{
		this.startPos = val;
	}
	private Point finishPos;
	public Point getFinishPos()
	{
		return this.finishPos;
	}
	public void setFinishPos(Point val)
	{
		this.finishPos = val;
	}
	private int data[][];
	private int keysCount;
	public int getFinishesCount()
	{
		return this.keysCount;
	}
	public void setFinishesCount(int val)
	{
		this.keysCount = val;
	}
	private LabyrinthKey keys[];	
//////////////////////////////////////////////////////////////////////
	public void enableAnimation(boolean value)
	{
		this.tack.enableAnimation(value);
	}
	
	private boolean sndEnable = true;
	public void enableSound(boolean value)
	{
		this.sndEnable = value;
	}
////////////////////////////////////////////////////////////////////
	private long currentTickCount = 0;
////////////////////////////////////////////////////////////////////	
	private Paint wallPaint;
	private Paint breakPaint;
	private Paint diodePaint;
	private Paint clearPaint;
	
	private Paint finishPaint;
	private Paint checkFinishPaint;
	
	private Paint movesCountPaint;
//////////////////////////////////////////////////////////////////////
	private Tack tack;
	private int movesCount = 0;
	private boolean justLoaded = false;
	public boolean getJustLoaded()
	{		
		boolean res = this.justLoaded;
		this.justLoaded = false;
		return res;
	}
	public void setJustLoaded(boolean val)
	{
		this.justLoaded = val;
	}
	private boolean toUsual = false;
	private Point toUsualPos = new Point(0, 0);
	private boolean toBreak_l = false;
	private boolean toBreak_r = false;
	private boolean toBreak_d = false;
	private boolean toBreak_u = false;
	private Point toBreakPos = new Point(0, 0);
	
	private boolean playWall = false;
	private boolean playKey = false;
	
	private boolean playedComplete = false;
	
	private boolean keysOk = false;
	private boolean onDoor = false;
	private boolean doorOpened = false;
	private boolean endLevel = false;
	
	public boolean isLevelCompleted()
	{
		return this.endLevel;
	}
	private String levelName = "";
	public String getLevelName()
	{
		return this.levelName;
	}
	private int levelNumber = 1;
	public int getLevelNumber()
	{
		return this.levelNumber;
	}
	public void setLevelNumber(int num)
	{
		this.levelNumber = num;
	}
	private int reachedLevel = 1;
	public int getReachedLevel()
	{
		return this.reachedLevel;
	}
	public final static int FINAL_LEVEL = 40;
//////////////////////////////////////////////////////////////////////
	private BitmapDrawable wallBMP;
	private BitmapDrawable wall_vBMP;
	private BitmapDrawable floorBMP;
	private BitmapDrawable doorBMP;
	private BitmapDrawable door_openBMP;
	private BitmapDrawable keyBMP;
	private BitmapDrawable glassBMP;
	private BitmapDrawable glass_vBMP;
	private BitmapDrawable brokenGlassBMP;
	private BitmapDrawable diodeBMP;
	private BitmapDrawable diode_vBMP;
	private BitmapDrawable diodeInvBMP;
	private BitmapDrawable diodeInv_vBMP;
	
	public static final int SOUND_BREAK = 1;
	public static final int SOUND_WALL = 2;
	public static final int SOUND_KEY = 3;
	public static final int SOUND_COMPLETE = 4;
	
	private SoundPool soundPool;
	private HashMap <Integer, Integer> soundPoolMap;
	private Context context;
	
	private void initSound()
	{
		this.soundPool = new SoundPool(4, AudioManager.STREAM_MUSIC, 0);		
		this.soundPoolMap = new HashMap<Integer, Integer>();
		this.soundPoolMap.put(Labyrinth.SOUND_WALL, this.soundPool.load(this.context, R.raw.snd_wall, 1));
		this.soundPoolMap.put(Labyrinth.SOUND_BREAK, this.soundPool.load(this.context, R.raw.snd_crash, 1));
		this.soundPoolMap.put(Labyrinth.SOUND_KEY, this.soundPool.load(this.context, R.raw.snd_key, 1));
		this.soundPoolMap.put(Labyrinth.SOUND_COMPLETE, this.soundPool.load(this.context, R.raw.snd_complete, 1));
	}
	
	public void releaseRes()
	{
		this.soundPoolMap.clear();
		this.soundPool.release();
	}
	
	public void playSound(int sndID)
	{
		if (!this.sndEnable)
			return;
		
		AudioManager mgr = (AudioManager)this.context.getSystemService(Context.AUDIO_SERVICE);
		int volume = mgr.getStreamVolume(AudioManager.STREAM_MUSIC);
		this.soundPool.play(this.soundPoolMap.get(sndID), volume, volume, 1, 0, 1.0f);
	}
	
	public Labyrinth(Context con)
	{
		this.context = con;
		
		this.wallPaint = new Paint();
        this.wallPaint.setColor(Color.WHITE);
        this.wallPaint.setStrokeWidth(3);
        this.wallPaint.setStyle(Style.STROKE);
        
        this.finishPaint = new Paint();
        this.finishPaint.setColor(Color.CYAN);
        this.finishPaint.setStyle(Style.FILL);
		
		this.checkFinishPaint = new Paint();
        this.checkFinishPaint.setColor(Color.YELLOW);
        this.checkFinishPaint.setStyle(Style.FILL);
        
        this.breakPaint = new Paint();
        this.breakPaint.setColor(Color.RED);
        this.breakPaint.setStrokeWidth(3);
        this.breakPaint.setStyle(Style.STROKE);
        
        this.diodePaint = new Paint();
        this.diodePaint.setColor(Color.BLUE);
        this.diodePaint.setStrokeWidth(2);
        this.diodePaint.setStyle(Style.STROKE);
        
        this.movesCountPaint = new Paint();
        this.movesCountPaint.setColor(Color.BLUE);
        this.movesCountPaint.setTextSize(20);
        this.movesCountPaint.setStrokeWidth(2);
        this.movesCountPaint.setStyle(Style.FILL);
        this.movesCountPaint.setTextAlign(Paint.Align.CENTER);
        
        DashPathEffect dashPath = new DashPathEffect(new float[]{5,5}, 1);
        this.clearPaint = new Paint();
        this.clearPaint.setColor(Color.WHITE);
        this.clearPaint.setStrokeWidth(1);
        this.clearPaint.setStyle(Style.STROKE);
        this.clearPaint.setPathEffect(dashPath);             
        
        this.res = con.getResources();
        this.tack = new Tack(res);
        this.wallBMP = (BitmapDrawable)this.res.getDrawable(R.drawable.wall);
        this.wall_vBMP = (BitmapDrawable)this.res.getDrawable(R.drawable.wall_v);
        this.floorBMP = (BitmapDrawable)this.res.getDrawable(R.drawable.floor);
        this.doorBMP = (BitmapDrawable)this.res.getDrawable(R.drawable.door);
        this.door_openBMP = (BitmapDrawable)this.res.getDrawable(R.drawable.door_open);
        this.keyBMP = (BitmapDrawable)this.res.getDrawable(R.drawable.key);
        this.glassBMP = (BitmapDrawable)this.res.getDrawable(R.drawable.glass);
        this.glass_vBMP = (BitmapDrawable)this.res.getDrawable(R.drawable.glass_v);
        this.brokenGlassBMP = (BitmapDrawable)this.res.getDrawable(R.drawable.broken);
        this.diodeBMP = (BitmapDrawable)this.res.getDrawable(R.drawable.diode);
        this.diode_vBMP = (BitmapDrawable)this.res.getDrawable(R.drawable.diode_v);
        this.diodeInvBMP = (BitmapDrawable)this.res.getDrawable(R.drawable.diode_inv);
        this.diodeInv_vBMP = (BitmapDrawable)this.res.getDrawable(R.drawable.diode_inv_v);
        
        this.initSound();
	}
	
	public void setCellType(CellType type, int i, int j)
	{	
		if (this.data == null)
			return;
		
		this.data[i][j] &= 0xFFF0FFFF;
		this.data[i][j] |= (type.getMask() << 16);
	}
	
	public CellType getCellType(int i, int j)
	{	
		if (this.data == null)
			return CellType.Unknown;
		
		return CellType.getByMask((this.data[i][j] >> 16) & 0xF);
	}
	
	public void setWallKind(WallType type, WallKind kind, int i, int j)
	{	
		if (this.data == null)
			return;
		
		this.data[i][j] &= ~(0xF << type.getOffset());
		this.data[i][j] |= (kind.getMask() << type.getOffset());
	}
	
	public WallKind getWallKind(WallType type, int i, int j)
	{
		if (this.data == null)
			return WallKind.Clear;
		
		return WallKind.getByMask((this.data[i][j] >> type.getOffset()) & 0xF);
	}
	
	public boolean checkDoorOpened(int i, int j)
	{
		return ((this.getCellType(i, j) == CellType.Finish) && (this.keysOk));
	}
	
	public boolean checkKeysOk(int i, int j)
	{
		if (this.getCellType(i, j) == CellType.Key)
		{			
			this.playKey = true;
			this.toUsual = true;
			this.toUsualPos.x = i;
			this.toUsualPos.y = j;
			
			for (int k = 0; k < this.keysCount; k++)
				if ((this.keys[k].pos.x == i) && (this.keys[k].pos.y == j))
					this.keys[k].collected = true;
		}
		
		for (int k = 0; k < this.keysCount; k++)
			if (!this.keys[k].collected)
				return false;
		
		return true;
	}
	
	private void getLevelData()
	{
		this.setLabyrinthWH(this.mLevelsIOProvider.getLabWidth(), 
							this.mLevelsIOProvider.getLabHeight());
		this.startPos = this.mLevelsIOProvider.getStartPos();
		this.finishPos = this.mLevelsIOProvider.getFinishPos();
		this.keysCount = this.mLevelsIOProvider.getKeysCount();			
		
		for (int i = 0; i < this.height; i++)
		for (int j = 0; j < this.width; j++)
			this.data[i][j] = this.mLevelsIOProvider.getLabData(i, j);
		
		this.keys = new LabyrinthKey[this.keysCount];
		for (int i = 0; i < this.keysCount; i++)
			this.keys[i] = new LabyrinthKey(this.mLevelsIOProvider.getKeyPos(i));
		
		this.movesCount = 0;
		this.endLevel = false;
		this.onDoor = false;
		this.doorOpened = this.keysOk = (this.keysCount == 0);
	}
	
	public boolean loadNextLevel()
	{
		if (this.loadLevel(this.levelNumber + 1))
		{
			this.tack.setLabPos(this.getStartPos());
			return true;
		}
		
		return false;
	}
	
	public boolean loadLevel(int levelNum)
	{
		this.levelName = Integer.toString(levelNum);
		this.levelNumber = levelNum;
		
		this.mLevelsIOProvider = new LevelsIOProvider("");
		
		if (!this.mLevelsIOProvider.loadLevelFromResource(this.res, R.raw.lev001 + levelNumber - 1))
			return false;
		
		this.getLevelData();
		
		this.playedComplete = false;
		
		return (this.justLoaded = true);
	}
	
	public boolean loadReachedLevel(Context con)
	{
		this.playedComplete = false;
		
		this.reachedLevel = this.mLevelsIOProvider.loadProgress(con);
		return this.loadLevel(this.reachedLevel);
	}
	
	public void saveReachedLevel(Context con)
	{
		if (this.levelNumber > this.reachedLevel)
		{
			this.reachedLevel = this.levelNumber;
			this.mLevelsIOProvider.saveProgress(con, this.reachedLevel);
		}
	}
	
	public boolean loadLevelEx(String levName)
	{
		this.levelName = "";
		this.playedComplete = false;
		
		this.mLevelsIOProvider = new LevelsIOProvider(this.levelsPath);
		
		if (!this.mLevelsIOProvider.loadLevelFromFile(levName + ".lab"))
			return false;
		
		this.getLevelData();	
		
		return (this.justLoaded = true);
	}
	
	public void MakeTurn(int dX, int dY)
	{		
		if (this.data == null)
			return;
		
		if ((!this.tack.isStopped()) || (this.onDoor))
			return;		
		
		Point res = new Point(this.tack.getLabPos());
		
		if (dX < 0)
		{
			while ((this.getWallKind(WallType.Left, res.y, res.x) != WallKind.Usual) &&
				   (this.getWallKind(WallType.Left, res.y, res.x) != WallKind.InverseDiode))
			{
				if (this.getWallKind(WallType.Left, res.y, res.x) == WallKind.Breakable)
				{
					this.toBreak_l = true;
					this.toBreakPos.x = res.y;
					this.toBreakPos.y = res.x;
					break;
				}
				else
					this.playWall = !this.toBreak_l;
								
				this.keysOk = this.checkKeysOk(res.x, res.y);
				this.onDoor = this.checkDoorOpened(res.x, res.y);
				res.x--;
			}	
			this.keysOk = this.checkKeysOk(res.x, res.y);
			this.onDoor = this.checkDoorOpened(res.x, res.y);
			
			if (res.x < 0)
				return;
		}
		
		if (dX > 0)
		{
			while ((this.getWallKind(WallType.Right, res.y, res.x) != WallKind.Usual) &&
				   (this.getWallKind(WallType.Right, res.y, res.x) != WallKind.InverseDiode))
			{
				if (this.getWallKind(WallType.Right, res.y, res.x) == WallKind.Breakable)
				{
					this.toBreak_r = true;
					this.toBreakPos.x = res.y;
					this.toBreakPos.y = res.x;
					break;
				}
				else
					this.playWall = !this.toBreak_r;
								
				this.keysOk = this.checkKeysOk(res.x, res.y);
				this.onDoor = this.checkDoorOpened(res.x, res.y);
				res.x++;
			}
			this.keysOk = this.checkKeysOk(res.x, res.y);
			this.onDoor = this.checkDoorOpened(res.x, res.y);
			
			if (res.x > this.width)
				return;
		}
		
		if (dY < 0)
		{
			while ((this.getWallKind(WallType.Top, res.y, res.x) != WallKind.Usual) &&
				   (this.getWallKind(WallType.Top, res.y, res.x) != WallKind.InverseDiode))		
			{
				if (this.getWallKind(WallType.Top, res.y, res.x) == WallKind.Breakable)
				{
					this.toBreak_u = true;
					this.toBreakPos.x = res.y;
					this.toBreakPos.y = res.x;
					break;
				}
				else
					this.playWall = !this.toBreak_u;
								
				this.keysOk = this.checkKeysOk(res.x, res.y);
				this.onDoor = this.checkDoorOpened(res.x, res.y);
				res.y--;
			}
			this.keysOk = this.checkKeysOk(res.x, res.y);
			this.onDoor = this.checkDoorOpened(res.x, res.y);
			
			if (res.y < 0)
				return;
		}
		
		if (dY > 0)
		{
			while ((this.getWallKind(WallType.Bottom, res.y, res.x) != WallKind.Usual) &&
				   (this.getWallKind(WallType.Bottom, res.y, res.x) != WallKind.InverseDiode))
			{
				if (this.getWallKind(WallType.Bottom, res.y, res.x) == WallKind.Breakable)
				{
					this.toBreak_d = true;
					this.toBreakPos.x = res.y;
					this.toBreakPos.y = res.x;
					break;
				}
				else
					this.playWall = !this.toBreak_d;
				
				this.keysOk = this.checkKeysOk(res.x, res.y);
				this.onDoor = this.checkDoorOpened(res.x, res.y);
				res.y++;				
			}									
			this.keysOk = this.checkKeysOk(res.x, res.y);
			this.onDoor = this.checkDoorOpened(res.x, res.y);
			
			if (res.y > this.height)
				return;
		}
		
		if ((res.x != this.tack.getLabPos().x) || (res.y != this.tack.getLabPos().y))
			this.movesCount++;
		
		this.tack.setLabPos(res);
	}
	
	private void DrawWall(Canvas canvas, WallKind kind, int i, int j, int cellSize, int leftLab, int topLab, Paint paint)
	{
		int diodeHalfSize = (int)((float)cellSize / ((float)this.diodeBMP.getBitmap().getWidth() / (float)this.diodeBMP.getBitmap().getHeight()) / 2.0f);
		
		if (this.getWallKind(WallType.Left, j, i) == kind)
		{			
			if (kind == WallKind.Usual)
    		{
				this.wall_vBMP.setBounds(i * cellSize + leftLab - 3, j * cellSize + topLab, i * cellSize + leftLab + 3, (j + 1) * cellSize + topLab);
				this.wall_vBMP.draw(canvas);
    		}
			
			if (kind == WallKind.Breakable)
    		{
				this.glass_vBMP.setBounds(i * cellSize + leftLab - 3, j * cellSize + topLab, i * cellSize + leftLab + 3, (j + 1) * cellSize + topLab);
				this.glass_vBMP.draw(canvas);
    		}
			
			if (kind == WallKind.Break)
    		{
				this.brokenGlassBMP.setBounds(i * cellSize + leftLab - (cellSize / 2), j * cellSize + topLab, i * cellSize + leftLab + (cellSize / 2), (j + 1) * cellSize + topLab);
				this.brokenGlassBMP.draw(canvas);
    		}

			if (kind == WallKind.Diode)
			{				
				this.diode_vBMP.setBounds(i * cellSize + leftLab - diodeHalfSize, j * cellSize + topLab, i * cellSize + leftLab + diodeHalfSize, (j + 1) * cellSize + topLab);
				this.diode_vBMP.draw(canvas);
			}
			
			if (kind == WallKind.InverseDiode)
			{
				this.diodeInv_vBMP.setBounds(i * cellSize + leftLab - diodeHalfSize, j * cellSize + topLab, i * cellSize + leftLab + diodeHalfSize, (j + 1) * cellSize + topLab);
				this.diodeInv_vBMP.draw(canvas);
			}
		}
    	if ((this.getWallKind(WallType.Right, j, i) == kind) && (i == this.getWidth() - 1))
    	{
    		if (kind == WallKind.Usual)
    		{
    			this.wall_vBMP.setBounds((i + 1) * cellSize + leftLab - 3, j * cellSize + topLab, (i + 1) * cellSize + leftLab + 3, (j + 1) * cellSize + topLab);
    			this.wall_vBMP.draw(canvas);
    		}
    		
    		if (kind == WallKind.Breakable)
    		{
    			this.glass_vBMP.setBounds((i + 1) * cellSize + leftLab - 3, j * cellSize + topLab, (i + 1) * cellSize + leftLab + 3, (j + 1) * cellSize + topLab);
    			this.glass_vBMP.draw(canvas);
    		}
    		
    		if (kind == WallKind.Break)
    		{
    			this.brokenGlassBMP.setBounds((i + 1) * cellSize + leftLab - (cellSize / 2), j * cellSize + topLab, (i + 1) * cellSize + leftLab + (cellSize / 2), (j + 1) * cellSize + topLab);
    			this.brokenGlassBMP.draw(canvas);
    		}
    	}
    	if (this.getWallKind(WallType.Top, j, i) == kind)
    	{
    		if (kind == WallKind.Usual)
    		{
    			this.wallBMP.setBounds(i * cellSize + leftLab, j * cellSize + topLab - 3, (i + 1) * cellSize + leftLab, j * cellSize + topLab + 3);
    			this.wallBMP.draw(canvas);
    		}
    		
    		if (kind == WallKind.Breakable)
    		{
    			this.glassBMP.setBounds(i * cellSize + leftLab, j * cellSize + topLab - 3, (i + 1) * cellSize + leftLab, j * cellSize + topLab + 3);
    			this.glassBMP.draw(canvas);
    		}
    		
    		if (kind == WallKind.Break)
    		{
    			this.brokenGlassBMP.setBounds(i * cellSize + leftLab, j * cellSize + topLab - (cellSize / 2), (i + 1) * cellSize + leftLab, j * cellSize + topLab + (cellSize / 2));
    			this.brokenGlassBMP.draw(canvas);
    		}

    		if (kind == WallKind.Diode)
			{				
    			this.diodeBMP.setBounds(i * cellSize + leftLab, j * cellSize + topLab - diodeHalfSize, (i + 1) * cellSize + leftLab, j * cellSize + topLab + diodeHalfSize);
				this.diodeBMP.draw(canvas);
			}
			
			if (kind == WallKind.InverseDiode)
			{
				this.diodeInvBMP.setBounds(i * cellSize + leftLab, j * cellSize + topLab - diodeHalfSize, (i + 1) * cellSize + leftLab, j * cellSize + topLab + diodeHalfSize);
				this.diodeInvBMP.draw(canvas);
			}
		}
    	if ((this.getWallKind(WallType.Bottom, j, i) == kind) && (j == this.getHeight() - 1))
    	{
    		if (kind == WallKind.Usual)
    		{
    			this.wallBMP.setBounds(i * cellSize + leftLab, (j + 1) * cellSize + topLab - 3, (i + 1) * cellSize + leftLab, (j + 1) * cellSize + topLab + 3);
    			this.wallBMP.draw(canvas);
    		}
    		
    		if (kind == WallKind.Breakable)
    		{
    			this.glassBMP.setBounds(i * cellSize + leftLab, (j + 1) * cellSize + topLab - 3, (i + 1) * cellSize + leftLab, (j + 1) * cellSize + topLab + 3);
    			this.glassBMP.draw(canvas);
    		}
    		
    		if (kind == WallKind.Break)
    		{
    			this.brokenGlassBMP.setBounds(i * cellSize + leftLab, (j + 1) * cellSize + topLab - (cellSize / 2), (i + 1) * cellSize + leftLab, (j + 1) * cellSize + topLab + (cellSize / 2));
    			this.brokenGlassBMP.draw(canvas);
    		}    		
		}
	}
	
	public void resetLevel()
	{
		this.getLevelData();
		
		this.movesCount = 0;
		this.tack.setLabPos(this.startPos);				
	}
	
	public void Draw(Canvas canvas, int screenW, int screenH)
	{		
		int cellSize = 0;
		int leftLab = 0;
		int topLab = 0;
		
		if (screenW < screenH)
		{
			cellSize = (screenW - this.offset * 2) / this.width;
			leftLab = this.offset;
			topLab = (screenH / 2) - (this.height * cellSize / 2);
			//topLab = screenH - (cellSize * this.height) - this.offset;
		}
		else
		{
    		//screenH =  (int)((float)screenH * 5.0f / 6.0f);
			cellSize = (screenH - this.offset * 2) / this.height;
			leftLab = (screenW / 2) - (this.width * cellSize / 2);
			//topLab = (int)((float)screenH / 6.0f) + this.offset;			
			topLab = screenH - (cellSize * this.height) - this.offset;
		}
		
		for (int i = 0; i < this.width; i++)
            for (int j = 0; j < this.height; j++)
            {                	
            	if (this.getCellType(i, j) == CellType.Finish)
            	{
        			if (this.doorOpened)
        			{
        				this.door_openBMP.setBounds(i * cellSize + leftLab, j * cellSize + topLab, (i + 1) * cellSize + leftLab, (j + 1) * cellSize + topLab);
        				this.door_openBMP.draw(canvas);
        			}
        			else
        			{
        				this.doorBMP.setBounds(i * cellSize + leftLab, j * cellSize + topLab, (i + 1) * cellSize + leftLab, (j + 1) * cellSize + topLab);
        				this.doorBMP.draw(canvas);
        			}
            	}
            	if (this.getCellType(i, j) == CellType.Key)
            	{
            		this.keyBMP.setBounds(i * cellSize + leftLab, j * cellSize + topLab, (i + 1) * cellSize + leftLab, (j + 1) * cellSize + topLab);
            		this.keyBMP.draw(canvas);
            	}
            	
            	if ((this.getCellType(i, j) == CellType.Usual) || (this.getCellType(i, j) == CellType.Unknown))
            	{
            		this.floorBMP.setBounds(i * cellSize + leftLab, j * cellSize + topLab, (i + 1) * cellSize + leftLab, (j + 1) * cellSize + topLab);
            		this.floorBMP.draw(canvas);
            	}        	            	
            	
            	this.DrawWall(canvas, WallKind.Diode, i, j, cellSize, leftLab, topLab, this.diodePaint);
            	this.DrawWall(canvas, WallKind.InverseDiode, i, j, cellSize, leftLab, topLab, this.diodePaint);
            	this.DrawWall(canvas, WallKind.Usual, i, j, cellSize, leftLab, topLab, this.wallPaint);
            	this.DrawWall(canvas, WallKind.Breakable, i, j, cellSize, leftLab, topLab, this.breakPaint);
            	this.DrawWall(canvas, WallKind.Break, i, j, cellSize, leftLab, topLab, this.clearPaint);
            }
							
		if (Math.abs(this.currentTickCount - SystemClock.uptimeMillis()) > 60)			
		{
			this.tack.updatePos();
			this.currentTickCount = SystemClock.uptimeMillis();
		}
		this.tack.Draw(canvas, cellSize, leftLab, topLab, this.width);
		
		if (this.movesCount == 0)
			this.tack.setLabPos_force(this.startPos);
    	
    	this.endLevel = this.keysOk && this.onDoor && this.tack.isStopped();
    	
    	if ((this.endLevel) && (!this.playedComplete))
    	{
    		this.playedComplete = true;
    		this.playSound(Labyrinth.SOUND_COMPLETE);
    	}
    	
    	Point realPos = this.tack.getRealPos();
    	if (this.toUsual)
    	{
    		if (this.tack.isAnimated())
    		{
		    	if ((realPos.x == this.toUsualPos.x) && (realPos.y == this.toUsualPos.y))
		    	{
		    		this.setCellType(CellType.Usual, this.toUsualPos.x, this.toUsualPos.y);
		    		this.toUsual = false;
		    		this.doorOpened = this.keysOk;
		    	}
    		}
    		else
    		{
		    	this.setCellType(CellType.Usual, this.toUsualPos.x, this.toUsualPos.y);
		    	this.toUsual = false;
		    	this.doorOpened = this.keysOk;
    		}
    	}
    	
    	if ((this.toBreak_l) && this.tack.isStopped())
    	{
    		this.setWallKind(WallType.Right, WallKind.Break, this.toBreakPos.x, this.toBreakPos.y - 1);
			this.setWallKind(WallType.Left, WallKind.Break, this.toBreakPos.x, this.toBreakPos.y);
    		this.toBreak_l = false;
    		this.playSound(Labyrinth.SOUND_BREAK);
    	}
    	
    	if ((this.toBreak_r) && this.tack.isStopped())
    	{
    		this.setWallKind(WallType.Right, WallKind.Break, this.toBreakPos.x, this.toBreakPos.y);
			this.setWallKind(WallType.Left, WallKind.Break, this.toBreakPos.x, this.toBreakPos.y + 1);
    		this.toBreak_r = false;
    		this.playSound(Labyrinth.SOUND_BREAK);
    	}
    	
    	if ((this.toBreak_u) && this.tack.isStopped())
    	{
    		this.setWallKind(WallType.Top, WallKind.Break, this.toBreakPos.x, this.toBreakPos.y);
			this.setWallKind(WallType.Bottom, WallKind.Break, this.toBreakPos.x - 1, this.toBreakPos.y);
    		this.toBreak_u = false;
    		this.playSound(Labyrinth.SOUND_BREAK);
    	}
    	
    	if ((this.toBreak_d) && this.tack.isStopped())
    	{
    		this.setWallKind(WallType.Top, WallKind.Break, this.toBreakPos.x + 1, this.toBreakPos.y);
			this.setWallKind(WallType.Bottom, WallKind.Break, this.toBreakPos.x, this.toBreakPos.y);
    		this.toBreak_d = false;
    		this.playSound(Labyrinth.SOUND_BREAK);
    	}
    	
    	if ((this.playKey) && this.tack.isStopped())
    	{
    		this.playSound(Labyrinth.SOUND_KEY);
    		this.playKey = false;
    		this.playWall = false;
    	}
    	
    	if ((this.playWall) && this.tack.isStopped() && !this.endLevel)
    	{
    		this.playSound(Labyrinth.SOUND_WALL);
    		this.playKey = false;
    		this.playWall = false;
    	}
	}
}