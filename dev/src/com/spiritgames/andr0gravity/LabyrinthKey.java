package com.spiritgames.andr0gravity;

import android.graphics.Point;

public class LabyrinthKey 
{
	public Point pos;
	public boolean collected;
	
	public LabyrinthKey(Point p)
	{
		this.pos = p;
		this.collected = false;
	}	
}
