package com.spiritgames.andr0gravity;

import android.content.res.Resources;
import android.graphics.*;
import android.graphics.Paint.Style;
import android.graphics.drawable.BitmapDrawable;

public class Tack 
{
	private BitmapDrawable currentBMP;
	
	private BitmapDrawable staticBMP;
	private BitmapDrawable leftMoveBMP;
	private BitmapDrawable rightMoveBMP;
	private BitmapDrawable downMoveBMP;
	
	private Point labPos;
	private Point oldLabPos;
	private Point curPos;
	private Point distPos;
	
	private int leftLab;
	private int topLab;
	private int cellSize;
	private int size;

	private boolean animate = false;
	public void enableAnimation(boolean value)
	{
		this.animate = value;
	}
	public boolean isAnimated()
	{
		return this.animate;
	}
	public boolean isStopped()
	{
		return ((this.curPos.x == this.distPos.x) && (this.curPos.y == this.distPos.y));
	}
	private int startSpeed = 1;
	private int speed = 1;
	private int velocity = 3;
	
	public Point getRealPos()
	{
		return new Point((this.curPos.x - this.leftLab) / this.cellSize, (this.curPos.y - this.topLab) / this.cellSize);		
	}
	
	public Point getLabPos()
	{
		return this.labPos;
	}
	public Point getOldLabPos()
	{
		return this.oldLabPos;
	}
	public void setLabPos(Point p)
	{
		this.oldLabPos.x = this.labPos.x;
		this.oldLabPos.y = this.labPos.y;
		
		this.labPos = p;
		
		this.distPos.x = this.labPos.x * this.cellSize + ((this.cellSize - this.size) / 2) + this.leftLab;
		this.distPos.y = this.labPos.y * this.cellSize + ((this.cellSize - this.size) / 2) + this.topLab;
		
		if (!this.animate)
		{
			this.curPos.x = this.distPos.x;
			this.curPos.y = this.distPos.y;
		}
	}
	public void setLabPos_force(Point p)
	{
		this.oldLabPos.x = this.labPos.x;
		this.oldLabPos.y = this.labPos.y;
		
		this.labPos = p;
		
		this.distPos.x = this.labPos.x * this.cellSize + ((this.cellSize - this.size) / 2) + this.leftLab;
		this.distPos.y = this.labPos.y * this.cellSize + ((this.cellSize - this.size) / 2) + this.topLab;

		this.curPos.x = this.distPos.x;
		this.curPos.y = this.distPos.y;
	}
		
	private Paint circlePaint;
	public Tack(Resources res)
	{		
		this.staticBMP = (BitmapDrawable)res.getDrawable(R.drawable.android);
		this.leftMoveBMP = (BitmapDrawable)res.getDrawable(R.drawable.android_l);
		this.rightMoveBMP = (BitmapDrawable)res.getDrawable(R.drawable.android_r);
		this.downMoveBMP = (BitmapDrawable)res.getDrawable(R.drawable.android_d);
		this.currentBMP = this.staticBMP;
		
		this.labPos = new Point(0, 0);
		this.oldLabPos = new Point(0, 0);
		this.curPos = new Point(-100, -100);
		this.distPos = new Point(-100, -100);
		
		this.circlePaint = new Paint();
        this.circlePaint.setColor(Color.RED);
        this.circlePaint.setStyle(Style.FILL);
	}	
	
	public void updatePos()
	{
		this.speed += this.velocity;
		
		if (Math.abs(this.curPos.x - this.distPos.x) < this.speed)
			this.curPos.x = this.distPos.x;
		
		if (Math.abs(this.curPos.y - this.distPos.y) < this.speed)
			this.curPos.y = this.distPos.y;
		
		if ((this.curPos.x == this.distPos.x) && (this.curPos.y == this.distPos.y))
		{
			this.speed = this.startSpeed;
			this.currentBMP = this.staticBMP;
		}
		
		if (this.curPos.x < this.distPos.x)
		{
			this.curPos.x += this.speed;
			this.currentBMP = this.rightMoveBMP;
		}
		
		if (this.curPos.x > this.distPos.x)
		{
			this.curPos.x -= this.speed;
			this.currentBMP = this.leftMoveBMP;
		}
		
		if (this.curPos.y < this.distPos.y)
		{
			this.curPos.y += this.speed;
			this.currentBMP = this.downMoveBMP;
		}
		
		if (this.curPos.y > this.distPos.y)
			this.curPos.y -= this.speed;
	}
	
	public void Draw(Canvas canvas, int cSize, int lLab, int tLab, int speedAlpha)
	{
		this.leftLab = lLab;
		this.topLab = tLab;
		this.cellSize = cSize;
		this.size = (int)((float)this.cellSize * 0.7f);
		
		this.currentBMP.setBounds(this.curPos.x, this.curPos.y, this.curPos.x + this.size, this.curPos.y + this.size);		
		this.currentBMP.draw(canvas);
	}
}
