package com.spiritgames.andr0gravity;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Point;
import android.util.Log;

public class LevelsIOProvider 
{
	private String levelsPath = "";	
	private InputStream reader;
	private FileOutputStream writer;
	
	private int labWidth;
	public int getLabWidth()
	{
		return this.labWidth;
	}
	private int labHeight;
	public int getLabHeight()
	{
		return this.labHeight;
	}
	public void setLabWH(int w, int h)
	{
		this.labWidth = w;
		this.labHeight = h;
		this.data = new int[w][h];
	}
	private Point startPos;
	public Point getStartPos()
	{
		return this.startPos;
	}
	public void setStartPos(Point val)
	{
		this.startPos = val;
	}
	private Point finishPos;
	public Point getFinishPos()
	{
		return this.finishPos;
	}
	public void setFinishPos(Point val)
	{
		this.finishPos = val;
	}
	private int data[][];
	public int getLabData(int i, int j)
	{
		return this.data[i][j];
	}
	private int keysCount;
	public int getKeysCount()
	{
		return this.keysCount;
	}
	public void setKeysCount(int val)
	{
		this.keysCount = val;
	}
	private Point keysPos[];
	public Point getKeyPos(int index)
	{
		return this.keysPos[index]; 
	}
	
	public LevelsIOProvider(String lPath)
	{
		this.levelsPath = lPath;
	}
	
	private boolean savePref(Context con, String filename, int val)
	{
		FileOutputStream fOut = null; 
	    OutputStreamWriter osw = null; 
		
		try 
		{	
			fOut = con.openFileOutput(filename, Context.MODE_PRIVATE);
			osw = new OutputStreamWriter(fOut);
	        osw.write(Integer.toString(val));
	        osw.flush(); 
		} 		
		 catch (Exception e) 
		 {       
	          e.printStackTrace(); 	    
	     }
		 finally
		 { 
             try 
             { 
            	 	if (osw != null)
            	 		osw.close();
            	 	
            	 	if (fOut != null)
            	 		fOut.close(); 
             }
             catch (Exception e) 
             { 
                    e.printStackTrace(); 
                    return false;
             } 
          } 
		
		return true;
	}	
	
	private int loadPref(Context con, String filename, String def)
	{	
		FileInputStream fIn = null; 
	    InputStreamReader isr = null; 
	      
	    char[] inputBuffer = new char[3]; 
	    String data = def;	    
	      
	    try
	    { 
	    	fIn = con.openFileInput(filename);       
	        isr = new InputStreamReader(fIn); 
	        isr.read(inputBuffer); 
	        data = new String(inputBuffer); 	          
	    } 
	    catch (Exception e) 
	    {       
	          e.printStackTrace();  
	    }
	    finally 
	    { 
	    	try
	    	{
	    		if (isr != null)
	    			isr.close();
	    		
	    		if (fIn != null)
	    			fIn.close(); 	            
	        } 
	    	catch (Exception e) 
	    	{ 
	        	e.printStackTrace(); 	        	
	    	} 
	    }
	    	
	    return Integer.parseInt(data.trim()); 
	}
	
	public boolean saveProgress(Context con, int val)
	{
		Log.d("saveProgress", Integer.toString(val));
		return this.savePref(con, "progress.lab", val);
	}
	
	public int loadProgress(Context con)
	{
		return this.loadPref(con, "progress.lab", "1");
	}
	
	public boolean saveAnimation(Context con, int val)
	{
		return this.savePref(con, "animation.lab", val);
	}
	
	public int loadAnimation(Context con)
	{
		return this.loadPref(con, "animation.lab", "1");
	}
	
	public boolean saveSound(Context con, int val)
	{
		return this.savePref(con, "sound.lab", val);
	}
	
	public int loadSound(Context con)
	{
		return this.loadPref(con, "sound.lab", "1");
	}
	
	public boolean saveSensor(Context con, int val)
	{
		return this.savePref(con, "sensor.lab", val);
	}
	
	public int loadSensor(Context con)
	{
		return this.loadPref(con, "sensor.lab", "1");
	}
	
	public boolean saveLevelToFile(String fileName)
	{
		try 
		{		
			this.writer = new FileOutputStream("/sdcard/" + this.levelsPath + "/" + fileName);
			
			this.saveLevel();
			
			this.writer.close();
		} 
		catch (FileNotFoundException e) 
		{					
			e.printStackTrace();
			return false;
		}
		catch (IOException e) 
		{		
			e.printStackTrace();
			return false;
		}  
		
		return true;
	}
	
	public boolean loadLevelFromFile(String fileName)
	{	
		try 
		{			
			this.reader = new FileInputStream("/sdcard/" + this.levelsPath + "/" + fileName);
			
			this.loadLevel();
			
			this.reader.close();
		} 
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
			return false;
		}
		catch (IOException e) 
		{		
			e.printStackTrace();
			return false;
		}  
		
		return true;
	}
	
	public boolean loadLevelFromResource(Resources res, int id)
	{
		try 
		{			
			this.reader = res.openRawResource(id);
			
			this.loadLevel();
			
			this.reader.close();
		} 
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
			return false;
		}
		catch (IOException e) 
		{		
			e.printStackTrace();
			return false;
		} 
		
		return true;
	}
	
	private void saveLevel()
	{
		this.writeByte(this.labWidth);
		this.writeByte(this.labHeight);
		
		this.writeByte(this.startPos.x);
		this.writeByte(this.startPos.y);
		
		this.writeByte(this.finishPos.x);
		this.writeByte(this.finishPos.y);
		
		for (int i = 0; i < this.labHeight; i++)
            for (int j = 0; j < this.labWidth; j++)
            	this.writeInt(this.data[i][j]);
	}
	
	private void loadLevel()
	{
		this.labWidth = (int)this.readByte(); 
		this.labHeight = (int)this.readByte();		
		
		int x = (int)this.readByte(); 
		int y = (int)this.readByte();
		
		this.startPos = new Point(x, y);
		
		x = (int)this.readByte(); 
		y = (int)this.readByte();
		
		this.finishPos = new Point(x, y);
		
		this.data = new int[this.labWidth][this.labHeight];
		
		this.keysCount = 0;
		for (int i = 0; i < this.labHeight; i++)
            for (int j = 0; j < this.labWidth; j++)
            {
            	this.data[i][j] = this.readInt();
            	if (this.getCellType(i, j) == CellType.Key)
            		this.keysCount++;
            }
				
		this.keysPos = new Point[this.keysCount];
		this.keysCount = 0;
		for (int i = 0; i < this.labHeight; i++)
            for (int j = 0; j < this.labWidth; j++)
            	if (this.getCellType(i, j) == CellType.Key)
            	{
            		this.keysPos[this.keysCount] = new Point(i, j);
            		this.keysCount++;
            	}
	}		 
	
	public void setCellType(CellType type, int i, int j)
	{	
		this.data[i][j] &= 0xFFF0FFFF;
		this.data[i][j] |= (type.getMask() << 16);
	}
	
	public CellType getCellType(int i, int j)
	{		
		return CellType.getByMask((this.data[i][j] >> 16) & 0xF);
	}
	
	public void setWallKind(WallType type, WallKind kind, int i, int j)
	{	
		this.data[i][j] &= ~(0xF << type.getOffset());
		this.data[i][j] |= (kind.getMask() << type.getOffset());
	}
	
	public WallKind getWallKind(WallType type, int i, int j)
	{			
		return WallKind.getByMask((this.data[i][j] >> type.getOffset()) & 0xF);
	}
	
	private void writeByte(int val)
	{
		try 
		{
			this.writer.write(val & 0xFF);
		}
		catch (IOException e) 
		{		
			e.printStackTrace();
			return;
		}
	}
	
	private void writeInt(int val)
	{
		this.writeByte(val & 0xFF);
		this.writeByte((val >> 8) & 0xFF);
		this.writeByte((val >> 16) & 0xFF);
		this.writeByte((val >> 24) & 0xFF);
	}
	
	private byte readByte()
	{
		try
		{
			return (byte)this.reader.read();
		}
		catch (IOException e) 
		{
			e.printStackTrace();
			return 0;
		}   
	}	
	private int readInt()
	{
		int byte1, byte2, byte3, byte4;
		
		try 
		{
			byte1 = this.reader.read();
			byte2 = this.reader.read();
			byte3 = this.reader.read();
			byte4 = this.reader.read();
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
			return 0;
		}        
        return ((byte4 << 24) | (byte3 << 16) | (byte2 << 8) | byte1);
	}	
}
