package com.spiritgames.andr0gravity;

import com.smaato.SOMA.SOMABanner;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.os.Looper;
import android.os.SystemClock;
import android.util.Log;
import android.view.Gravity;
import android.view.SurfaceHolder;
import android.widget.Toast;

public class GameRenderer extends Thread 
{
	class NotifyStartThread extends Thread
	{
		@Override 
		public void run()
		{
			Toast toast;
			
			Looper.prepare();
			toast = Toast.makeText(GameRenderer.this.context.getApplicationContext(), "Level " + GameRenderer.this.labyrinth.getLevelName(), Toast.LENGTH_SHORT);
			toast.setGravity(Gravity.CENTER, 0, 0);
			toast.show();
			Looper.loop();	
		}
	}
	private NotifyStartThread notifyStartThread = null;
	
	private SensorReader sensorReader;
	public void setSensorReader(SensorReader sensorReader)
	{
		this.sensorReader = sensorReader;
	}
	
    private SurfaceHolder mSurfaceHolder;
    public Context context;
    
    private int state = GameScreen.STATE_MENU;
    
    public Labyrinth labyrinth;
    private LevelsIOProvider mLevelsIOProvider;
    
    private int levelToLoad = 0;
    private boolean enableAnimation;
    private boolean enableSound;
    private boolean enableSensor;
    private boolean mRunning;
    private boolean mPause;
    private Bitmap mBackground;
    private BitmapDrawable mSpace;
    
    private SOMABanner mBanner;
    private long bannerTickCount = 0;
    
    public MMManager mManager;
    
    public GameRenderer(SurfaceHolder surfaceHolder, Context con)
    {
        this.mSurfaceHolder = surfaceHolder;
        this.mRunning = false;
        this.mPause = false;
             
        this.context = con;       
        
        System.gc();
        
        Resources res = this.context.getResources();        
        this.mSpace = (BitmapDrawable)res.getDrawable(R.drawable.space);
        
        this.mLevelsIOProvider = new LevelsIOProvider("");
        
        this.enableAnimation = (this.mLevelsIOProvider.loadAnimation(this.context) == 1);
        this.enableSound = (this.mLevelsIOProvider.loadSound(this.context) == 1);
        this.enableSensor = (this.mLevelsIOProvider.loadSensor(this.context) == 1);
        this.labyrinth = new Labyrinth(this.context);
        
        this.mManager = new MMManager(this.context);
    }
    
    public void setState(int val, int level)
    {        
    	this.state = val;    	
    	if (level > 0)
    		this.setLevelToLoad(level);
    	else
    		this.levelToLoad = 0;
    }
    
    public Rect getItemBounds(int item)
    { 
    	return this.mManager.getItemBounds(item);
    }
    
    public void setBanner(SOMABanner banner)
    {
    	this.mBanner = banner;
    }
    
    public int getScreenWidth()
    {
    	return this.mBackground.getWidth();
    }
    
    public int getScreenHeight()
    {
    	return this.mBackground.getHeight();
    }
    
    public void initBackBuffer(int screenWidth, int screenHeight)
    {
        this.mBackground = Bitmap.createBitmap(screenWidth, screenHeight, Bitmap.Config.RGB_565);   
    }    

    public void setRunning(boolean running)
    {
    	this.mRunning = running;
    }
    
    public void setPaused(boolean paused)
    {
    	this.mPause = paused;
    }
    
    public void setLevelToLoad(int val)
    {
    	int reachedLevel = this.mLevelsIOProvider.loadProgress(this.context.getApplicationContext());
    	if (reachedLevel < val)
    		this.mLevelsIOProvider.saveProgress(this.context.getApplicationContext(), val);
    	
    	this.enableAnimation = (this.mLevelsIOProvider.loadAnimation(this.context.getApplicationContext()) == 1);
        this.enableSound = (this.mLevelsIOProvider.loadSound(this.context.getApplicationContext()) == 1);
        this.enableSensor = (this.mLevelsIOProvider.loadSensor(this.context.getApplicationContext()) == 1);
    	
    	this.labyrinth.enableAnimation(this.enableAnimation);
    	this.labyrinth.enableSound(this.enableSound);
    	this.mManager.setSoundEnabled(this.enableSound);
    	this.mManager.setSensorEnabled(this.enableSensor);
    	
    	this.levelToLoad = val;
    }
    
    @Override
    public void run()
    {    	    	
        while (this.mRunning)
        {   
        	if (this.mPause)
        	{
				try 
				{
					Thread.sleep(1000);
				} 
				catch (InterruptedException e1) 
				{		
					e1.printStackTrace();
				}
        	}
        	
        	if ((this.levelToLoad > 0) && (this.state == GameScreen.STATE_GAME))
        	{        		
        		Log.d("GR", "levelToLoad=" + Integer.toString(this.levelToLoad));
        		this.labyrinth.loadLevel(this.levelToLoad);
        		this.levelToLoad = 0;

    			if (this.notifyStartThread != null)
    			{    			
    				this.notifyStartThread.interrupt();
    				this.notifyStartThread = null;
    			}
    			
    			this.notifyStartThread = new NotifyStartThread();
    			this.notifyStartThread.start();   
        	}
        	
            Canvas canvas = null;
            Canvas backCanvas = null; 
            
            int spaceW = 1;
            int spaceH = 1;
            
            try
            {            
            	if ((this.sensorReader != null) && (this.enableSensor) && (!this.labyrinth.getJustLoaded()))
            	{
            		float SENSOR_SENS = 17.0f;
            		
            		if (this.sensorReader.getYOrientation() > SENSOR_SENS)
            		{
            			this.labyrinth.MakeTurn(0, -1);
            		}
            		
            		if (this.sensorReader.getYOrientation() < -SENSOR_SENS)
            		{
            			this.labyrinth.MakeTurn(0, 1);
            		}
            		
            		if (this.sensorReader.getZOrientation() > SENSOR_SENS)
            		{
            			this.labyrinth.MakeTurn(-1, 0);
            		}
            		
            		if (this.sensorReader.getZOrientation() < -SENSOR_SENS)
            		{
            			this.labyrinth.MakeTurn(1, 0);
            		}
            	}

                canvas = this.mSurfaceHolder.lockCanvas();  
                
                                
                backCanvas = new Canvas(this.mBackground);                               
                
                synchronized (this.mSurfaceHolder)
                {         	
                	spaceW = this.mBackground.getWidth() / this.mSpace.getIntrinsicWidth() + 1;
                	spaceH = this.mBackground.getHeight() / this.mSpace.getIntrinsicHeight() + 1;
                	
                	if (this.mManager.getCurrentLayer() != MMManager.LAYER_SPIRIT_GAMES)
                	{
	                	for (int i = 0; i < spaceW; i++)
	                		for (int j = 0; j < spaceH; j++)
	                		{
	                			this.mSpace.setBounds(i * this.mSpace.getIntrinsicWidth(), j * this.mSpace.getIntrinsicHeight(), 
	                								 (i + 1) * this.mSpace.getIntrinsicWidth(), (j + 1) * this.mSpace.getIntrinsicHeight());
	                			this.mSpace.draw(backCanvas);
	                		}
                	}
                	
                	if (this.state == GameScreen.STATE_GAME)
                		this.labyrinth.Draw(backCanvas, this.mBackground.getWidth(), this.mBackground.getHeight());
                	
                	if (this.state == GameScreen.STATE_MENU)
                		this.mManager.Draw(backCanvas, this.mBackground.getWidth(), this.mBackground.getHeight(), this.mBanner);
                	
                	if (!((canvas == null) || (this.mBackground == null)))               	
                		canvas.drawBitmap(this.mBackground, 0, 0, null);
                	
                	if (Math.abs(this.bannerTickCount - SystemClock.uptimeMillis()) > 15000)			
            		{
                		this.mBanner.setBackgroundColor(Color.BLACK);
                		this.mBanner.setFontColor(Color.WHITE);
                		this.mBanner.fetchDrawableOnThread();
                		this.bannerTickCount = SystemClock.uptimeMillis();
            		}
                }
                
                if (this.labyrinth.isLevelCompleted())
				{						
					if (this.labyrinth.getLevelNumber() == Labyrinth.FINAL_LEVEL)
					{
						this.setState(GameScreen.STATE_MENU, 0);
						this.mManager.setCurrentLayer(MMManager.LAYER_THE_END);
						this.labyrinth.setLevelNumber(0);												
					}
					else
						this.setLevelToLoad(this.labyrinth.getLevelNumber() + 1);
				}
            }
            catch (Exception e) 
            { 
            	e.printStackTrace();
            }
            finally
            {
                if (canvas != null)
                {
                	this.mSurfaceHolder.unlockCanvasAndPost(canvas);                	
                }
            }                        
        }
    }
}

