package com.spiritgames.andr0gravity;

public enum WallKind 
{
	Usual(1),			// ������� ������
	Breakable(2),		// ������ (����������� ��� ������ ������������)
	Break(3),			// ��� �������� ������
	Diode(4),			// ������ ��������� �� ������
	InverseDiode(8),	// ������ �������� � ������
	Clear(0);			// ������ ���
	
	private int mask;
	
	WallKind(int m)
	{
		this.mask = m;
	}
	
	public int getMask() 
	{
		return this.mask;
	}
	
	public static WallKind getByMask(int m)
	{
		switch(m)
		{			
			case 1:
				return WallKind.Usual;
			case 2:
				return WallKind.Breakable;
			case 3:
				return WallKind.Break;
			case 4:
				return WallKind.Diode;
			case 8:
				return WallKind.InverseDiode;
			default:
				return WallKind.Clear;
		}		
	}
}
