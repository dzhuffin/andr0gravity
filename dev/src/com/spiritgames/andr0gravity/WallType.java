package com.spiritgames.andr0gravity;

public enum WallType 
{
	Left(12),
	Right(4),
	Top(8),
	Bottom(0);
	
	private int offset;
	
	WallType(int type)
	{
		this.offset = type;
	}
	
	public int getOffset() 
	{
		return this.offset;
	}
}