package com.spiritgames.andr0gravity;

import java.util.HashMap;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Point;
import android.graphics.Rect;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Bundle;
import android.os.PowerManager;
import android.util.Log;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.GestureDetector.OnGestureListener;
import android.view.View.OnKeyListener;
import android.view.View.OnTouchListener;

import com.smaato.SOMA.SOMABanner;

public class GameScreen extends Activity implements OnTouchListener, OnGestureListener, OnKeyListener, SensorEventListener
{
	//private SensorManager sensorManager;

	protected PowerManager.WakeLock wakeLock;
	private GameView gameView;
	private SOMABanner mBanner;
	private String pid;
	private String sid;

	private Point touchPoint;
	
	private LevelsIOProvider mLevelsIOProvider;
    
    private GestureDetector gestureScanner; 
    
	private int reachedLevel = 1;
	
	public final static int HINT_HISTORY = 1;
	public final static int HINT_KEYS = 20;
	public final static int HINT_WALLS = 8;
	public final static int HINT_CONTROLS = 1;
	
	public final static int REQUEST_GAME = 0;
	
	public final static int RESULT_EXIT = 5;
	public final static int RESULT_NEXT_LEVEL = 10;
	public final static int RESULT_FINISH_GAME = 1000;
	
	private int nextLevel = 0;
	
	private SoundPool soundPool;
	private HashMap <Integer, Integer> soundPoolMap;
	
	public static final int SOUND_CLICK = 1;
	public static final int SOUND_START = 2;
    
	private SensorReader sensorReader; 
    private boolean enableSensor;
    private boolean sensorAvalible;
    private float oldPitch = 0.0f;
	private float oldRoll = 0.0f;
	
	private int state = GameScreen.STATE_MENU;
	public final static int STATE_MENU = 0;
	public final static int STATE_GAME = 1;	
	
	class StartMusicThread extends Thread
	{
		@Override 
		public void run()
		{
			try 
			{
				Thread.sleep(7000);				
			} 
			catch (InterruptedException e) 
			{			
				e.printStackTrace();
			}
			finally
			{				
				GameScreen.this.playSound(GameScreen.SOUND_START);
			}
		}
	}
	private StartMusicThread startMusicThread;	
	
    public void onCreate(Bundle savedInstanceState) 
    {   	
        super.onCreate(savedInstanceState);
        setContentView(R.layout.game_screen);
        
     // ����� ����� �� ����������
        PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);  
        this.wakeLock = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK, "DoNotDimScreen");
        
     // ����� ������� ��������� ��������/���������� ��������� �����, � �� ������
        this.setVolumeControlStream(AudioManager.STREAM_MUSIC);
        
        this.sensorReader = new SensorReader(this);
        
        this.gameView = (GameView)this.findViewById(R.id.game);
        this.gameView.setLongClickable(true);
        this.gameView.setOnTouchListener(this);
        this.gameView.setOnKeyListener(this);
        this.gameView.setFocusable(true);
        this.gameView.setFocusableInTouchMode(true);       
        
        Resources res = this.getResources();                    
        
        this.pid = res.getString(R.string.pid);
        this.sid = res.getString(R.string.sid);
        
        this.mBanner = (SOMABanner)findViewById(R.id.GameScreenBannerView);        
        
        this.touchPoint = new Point(0, 0);
        
        this.mLevelsIOProvider = new LevelsIOProvider("");
        this.enableSensor = (this.mLevelsIOProvider.loadSensor(this.getApplicationContext()) == 1);
        
        this.gameView.mRenderer.mManager.setSoundEnabled((this.mLevelsIOProvider.loadSound(this.getApplicationContext()) == 1));
        this.gameView.mRenderer.mManager.setSensorEnabled((this.mLevelsIOProvider.loadSensor(this.getApplicationContext()) == 1));
        
        this.initSound();
        
        this.startMusicThread = new StartMusicThread(); 
        this.startMusicThread.start();
                
        this.gestureScanner = new GestureDetector(this);  
        this.gestureScanner.setIsLongpressEnabled(true);  
        
        SOMABanner.setPubID(this.pid);
        SOMABanner.setAdID(this.sid);
        SOMABanner.setMediaType("ALL");
        this.mBanner.setAutoRefresh(false);        
        this.mBanner.setAnimationOn(true);
        this.mBanner.setAnimationType(3);
        this.mBanner.setHideWhenError(false);
        this.gameView.mRenderer.setBanner(this.mBanner);
        this.gameView.mRenderer.setSensorReader(this.sensorReader);
    }
    
    private void initSound()
	{
		this.soundPool = new SoundPool(4, AudioManager.STREAM_MUSIC, 0);		
		this.soundPoolMap = new HashMap<Integer, Integer>();
		this.soundPoolMap.put(GameScreen.SOUND_CLICK, this.soundPool.load(this, R.raw.snd_mm_click, 1));
		this.soundPoolMap.put(GameScreen.SOUND_START, this.soundPool.load(this, R.raw.snd_mm_start, 1));
	}
	
	public void playSound(int sndID)
	{
		if (!this.gameView.mRenderer.mManager.getSoundEnabled())
			return;
			
		AudioManager mgr = (AudioManager)this.getSystemService(Context.AUDIO_SERVICE);
		int volume = mgr.getStreamVolume(AudioManager.STREAM_MUSIC);
		this.soundPool.play(this.soundPoolMap.get(sndID), volume, volume, 1, 0, 1.0f);
	}
    
	private void setState(int state, int level)
	{	 		
		this.gameView.mRenderer.setState(state, level);
		this.state = state;
	}
	
    @Override
    public void onStop() 
    {
        super.onStop();       
        
        this.sensorReader.unRegister();
    }
    
    @Override
    public void onResume() 
    {
        super.onResume();              
        
        this.gameView.mRenderer.resume();
        this.gameView.mRenderer.setPaused(false);
        
        this.wakeLock.acquire();
        
        this.mBanner.setAutoRefresh(true);
        
        this.reachedLevel = this.mLevelsIOProvider.loadProgress(this);
        
        this.oldPitch = 0.0f;
    	this.oldRoll = 0.0f;
        this.enableSensor = (this.mLevelsIOProvider.loadSensor(this.getApplicationContext()) == 1);
        
        if (this.enableSensor)
        	this.sensorReader.register();
    }
    
    @Override
    public void onPause() 
    {
        super.onPause();
        
        this.gameView.mRenderer.setPaused(true);
        this.gameView.mRenderer.suspend();        
        
        this.wakeLock.release();
        
        this.mBanner.setAutoRefresh(false);
    }
    
    @Override
    public void onDestroy() 
    {
        super.onDestroy();
        
        this.soundPool.release();
        
        this.gameView.mRenderer.labyrinth.releaseRes();
        
        this.startMusicThread.interrupt();
        this.gameView.mRenderer.setRunning(false);
    }
    
    private boolean mainMenuTouch(View v, MotionEvent event)
    {	
		Rect startBounds = this.gameView.mRenderer.getItemBounds(MMManager.ITEM_START);
		Rect settingsBounds = this.gameView.mRenderer.getItemBounds(MMManager.ITEM_SETTINGS);
		Rect exitBounds = this.gameView.mRenderer.getItemBounds(MMManager.ITEM_EXIT);
		Rect aboutBounds = this.gameView.mRenderer.getItemBounds(MMManager.ITEM_ABOUT);
		
		Rect levelMinusBounds = this.gameView.mRenderer.getItemBounds(MMManager.ITEM_LEVEL_MINUS);
		Rect levelPlusBounds = this.gameView.mRenderer.getItemBounds(MMManager.ITEM_LEVEL_PLUS);
		Rect levelBounds = this.gameView.mRenderer.getItemBounds(MMManager.ITEM_LEVEL);
		
		Rect aniBounds = this.gameView.mRenderer.getItemBounds(MMManager.ITEM_OPTIONS_ANIMATION);
		Rect sndBounds = this.gameView.mRenderer.getItemBounds(MMManager.ITEM_OPTIONS_SOUND);
		Rect senBounds = this.gameView.mRenderer.getItemBounds(MMManager.ITEM_OPTIONS_SENSOR);
		
		if (event.getAction() == MotionEvent.ACTION_DOWN)
		{
			if (this.gameView.mRenderer.mManager.getCurrentLayer() == MMManager.LAYER_MAIN)
			{
				if (startBounds.contains((int)event.getX(), (int)event.getY()))
				{
					this.gameView.mRenderer.mManager.setPressedItem(MMManager.ITEM_START);
					this.playSound(GameScreen.SOUND_CLICK);
				}
				if (settingsBounds.contains((int)event.getX(), (int)event.getY()))
				{
					this.gameView.mRenderer.mManager.setPressedItem(MMManager.ITEM_SETTINGS);
					this.playSound(GameScreen.SOUND_CLICK);
				}
				if (exitBounds.contains((int)event.getX(), (int)event.getY()))
				{
					this.gameView.mRenderer.mManager.setPressedItem(MMManager.ITEM_EXIT);
					this.playSound(GameScreen.SOUND_CLICK);
				}
				if (aboutBounds.contains((int)event.getX(), (int)event.getY()))
				{
					this.gameView.mRenderer.mManager.setPressedItem(MMManager.ITEM_ABOUT);
					this.playSound(GameScreen.SOUND_CLICK);
				}
			}
			
			if (this.gameView.mRenderer.mManager.getCurrentLayer() == MMManager.LAYER_LEVELS)
			{
				if (levelMinusBounds.contains((int)event.getX(), (int)event.getY()))
				{
					this.gameView.mRenderer.mManager.setPressedItem(MMManager.ITEM_LEVEL_MINUS);
					this.playSound(GameScreen.SOUND_CLICK);
				}
				if (levelPlusBounds.contains((int)event.getX(), (int)event.getY()))
				{
					this.gameView.mRenderer.mManager.setPressedItem(MMManager.ITEM_LEVEL_PLUS);
					this.playSound(GameScreen.SOUND_CLICK);
				}
				if (levelBounds.contains((int)event.getX(), (int)event.getY()))
				{
					this.gameView.mRenderer.mManager.setPressedItem(MMManager.ITEM_LEVEL);
					this.playSound(GameScreen.SOUND_CLICK);
				}
			}
			
			if (this.gameView.mRenderer.mManager.getCurrentLayer() == MMManager.LAYER_CONTEXT)
			{
				if (exitBounds.contains((int)event.getX(), (int)event.getY()))		// cancel pressed
				{
					this.gameView.mRenderer.mManager.setPressedItem(MMManager.ITEM_CONTEXT_CANCEL);
					this.playSound(GameScreen.SOUND_CLICK);
				}
				if (settingsBounds.contains((int)event.getX(), (int)event.getY()))	// restart pressed
				{
					this.gameView.mRenderer.mManager.setPressedItem(MMManager.ITEM_CONTEXT_RESTART);
					this.playSound(GameScreen.SOUND_CLICK);
				}
			}
			
			if (this.gameView.mRenderer.mManager.getCurrentLayer() == MMManager.LAYER_OPTIONS)
			{
				if (senBounds.contains((int)event.getX(), (int)event.getY()))
				{
					this.gameView.mRenderer.mManager.setSensorEnabled(!this.gameView.mRenderer.mManager.getSensorEnabled());
					this.playSound(GameScreen.SOUND_CLICK);
				}
				if (sndBounds.contains((int)event.getX(), (int)event.getY()))
				{
					this.gameView.mRenderer.mManager.setSoundEnabled(!this.gameView.mRenderer.mManager.getSoundEnabled());
					this.playSound(GameScreen.SOUND_CLICK);
				}
				if (aniBounds.contains((int)event.getX(), (int)event.getY()))
				{
					this.gameView.mRenderer.mManager.setAnimationEnabled(!this.gameView.mRenderer.mManager.getAnimationEnabled());
					this.playSound(GameScreen.SOUND_CLICK);
				}
			}
		}
		
		if (event.getAction() == MotionEvent.ACTION_UP)
		{
			if (this.gameView.mRenderer.mManager.getCurrentLayer() == MMManager.LAYER_MAIN)
			{
				if ((startBounds.contains((int)event.getX(), (int)event.getY())) && (this.gameView.mRenderer.mManager.getPressedItem() == MMManager.ITEM_START))
				{
					this.reachedLevel = this.mLevelsIOProvider.loadProgress(this.getApplicationContext());
					this.gameView.mRenderer.mManager.setReachedLevel(this.reachedLevel);
					//this.gameView.mRenderer.mManager.setReachedLevel(Labyrinth.FINAL_LEVEL);
					this.gameView.mRenderer.mManager.setCurrentLayer(MMManager.LAYER_LEVELS);
				}
				if ((settingsBounds.contains((int)event.getX(), (int)event.getY())) && (this.gameView.mRenderer.mManager.getPressedItem() == MMManager.ITEM_SETTINGS))
				{
					this.gameView.mRenderer.mManager.setAnimationEnabled(this.mLevelsIOProvider.loadAnimation(this) == 1);
					this.gameView.mRenderer.mManager.setSoundEnabled(this.mLevelsIOProvider.loadSound(this) == 1);
					this.gameView.mRenderer.mManager.setSensorEnabled(this.mLevelsIOProvider.loadSensor(this) == 1);
					this.gameView.mRenderer.mManager.setCurrentLayer(MMManager.LAYER_OPTIONS);
				}
				if ((exitBounds.contains((int)event.getX(), (int)event.getY())) && (this.gameView.mRenderer.mManager.getPressedItem() == MMManager.ITEM_EXIT))
				{
					this.finish();
					System.exit(0);
				}
				
				if ((aboutBounds.contains((int)event.getX(), (int)event.getY())) && (this.gameView.mRenderer.mManager.getPressedItem() == MMManager.ITEM_ABOUT))
				{
					this.gameView.mRenderer.mManager.setCurrentLayer(MMManager.LAYER_ABOUT);
					return gestureScanner.onTouchEvent(event);
				}
			}
			
			if (this.gameView.mRenderer.mManager.getCurrentLayer() == MMManager.LAYER_CONTEXT)
			{
				if ((settingsBounds.contains((int)event.getX(), (int)event.getY())) && (this.gameView.mRenderer.mManager.getPressedItem() == MMManager.ITEM_CONTEXT_RESTART))
				{
					this.gameView.mRenderer.labyrinth.resetLevel();
					this.setState(GameScreen.STATE_GAME, 0);
				}
				if ((exitBounds.contains((int)event.getX(), (int)event.getY())) && (this.gameView.mRenderer.mManager.getPressedItem() == MMManager.ITEM_CONTEXT_CANCEL))
				{
					this.setState(GameScreen.STATE_GAME, 0);
				}
			}
			
			if (this.gameView.mRenderer.mManager.getCurrentLayer() == MMManager.LAYER_LEVELS)
			{
				if ((levelMinusBounds.contains((int)event.getX(), (int)event.getY())) && (this.gameView.mRenderer.mManager.getPressedItem() == MMManager.ITEM_LEVEL_MINUS))
				{
					this.gameView.mRenderer.mManager.DecSelectedLevel();
				}
				
				if ((levelPlusBounds.contains((int)event.getX(), (int)event.getY())) && (this.gameView.mRenderer.mManager.getPressedItem() == MMManager.ITEM_LEVEL_PLUS))
				{
					this.gameView.mRenderer.mManager.IncSelectedLevel();
				}
				
				if ((levelBounds.contains((int)event.getX(), (int)event.getY())) && (this.gameView.mRenderer.mManager.getPressedItem() == MMManager.ITEM_LEVEL))
				{
					if (this.gameView.mRenderer.mManager.getSelectedLevel() == GameScreen.HINT_HISTORY)
					{
						this.gameView.mRenderer.mManager.setCurrentLayer(MMManager.LAYER_HISTORY);
						return gestureScanner.onTouchEvent(event);
					}	
					
					if (this.gameView.mRenderer.mManager.getSelectedLevel() == GameScreen.HINT_KEYS)
					{
						this.gameView.mRenderer.mManager.setCurrentLayer(MMManager.LAYER_KEYS);
						this.nextLevel = this.gameView.mRenderer.mManager.getSelectedLevel();
						return gestureScanner.onTouchEvent(event);
					}	
					
					if (this.gameView.mRenderer.mManager.getSelectedLevel() == GameScreen.HINT_WALLS)
					{
						this.gameView.mRenderer.mManager.setCurrentLayer(MMManager.LAYER_WALLS);
						this.nextLevel = this.gameView.mRenderer.mManager.getSelectedLevel();
						return gestureScanner.onTouchEvent(event);
					}	

	    			this.setState(GameScreen.STATE_GAME, this.gameView.mRenderer.mManager.getSelectedLevel());
				}
			}
			
			if (this.gameView.mRenderer.mManager.getCurrentLayer() == MMManager.LAYER_CONTROLS)
			{
				this.playSound(GameScreen.SOUND_CLICK);
				
				this.setState(GameScreen.STATE_GAME, this.gameView.mRenderer.mManager.getSelectedLevel());			
			}
			
			if (this.gameView.mRenderer.mManager.getCurrentLayer() == MMManager.LAYER_HISTORY)
			{
				this.playSound(GameScreen.SOUND_CLICK);
				this.gameView.mRenderer.mManager.setCurrentLayer(MMManager.LAYER_CONTROLS);			
			}
			
			if ((this.gameView.mRenderer.mManager.getCurrentLayer() == MMManager.LAYER_KEYS) ||
				(this.gameView.mRenderer.mManager.getCurrentLayer() == MMManager.LAYER_WALLS))
			{
				this.playSound(GameScreen.SOUND_CLICK);
    			
				this.setState(GameScreen.STATE_GAME, this.nextLevel);
			}
			
			if (this.gameView.mRenderer.mManager.getCurrentLayer() == MMManager.LAYER_ABOUT)
			{
				this.playSound(GameScreen.SOUND_CLICK);
				this.gameView.mRenderer.mManager.setCurrentLayer(MMManager.LAYER_MAIN);
			}
			
			this.gameView.mRenderer.mManager.setPressedItem(0);
		}			
		
		return gestureScanner.onTouchEvent(event);
    }
            
	public boolean gameTouch(View v, MotionEvent event) 
	{										
		if ((this.enableSensor) && (this.sensorAvalible))
			return false;
		
		if (event.getAction() == MotionEvent.ACTION_DOWN)
			this.touchPoint.set((int)event.getX(), (int)event.getY());
			
		if (event.getAction() == MotionEvent.ACTION_UP)
		{
			if (Math.abs((int)event.getX() - this.touchPoint.x) > Math.abs((int)event.getY() - this.touchPoint.y))
				this.gameView.mRenderer.labyrinth.MakeTurn((int)event.getX() - this.touchPoint.x, 0);
			else
				this.gameView.mRenderer.labyrinth.MakeTurn(0, (int)event.getY() - this.touchPoint.y);
		}
		
		return gestureScanner.onTouchEvent(event);
	}	
	
	public boolean onTouch(View v, MotionEvent event) 
	{										
		if (this.state == GameScreen.STATE_MENU)
			return this.mainMenuTouch(v, event);
		
		if (this.state == GameScreen.STATE_GAME)
			return this.gameTouch(v, event);
		
		return false;
	}

    private boolean gameKey(View v, int keyCode, KeyEvent event) 
    {
    	if (event.getAction() == KeyEvent.ACTION_DOWN)
    	{
	    	switch (keyCode)
			{
				case KeyEvent.KEYCODE_MENU:				
					this.setState(GameScreen.STATE_MENU, 0);
					this.gameView.mRenderer.mManager.setCurrentLayer(MMManager.LAYER_CONTEXT);
					return true;
				case KeyEvent.KEYCODE_DPAD_LEFT:
					if ((!this.enableSensor) || (!this.sensorAvalible))
						this.gameView.mRenderer.labyrinth.MakeTurn(-1, 0);
					return true;
				case KeyEvent.KEYCODE_DPAD_RIGHT:
					if ((!this.enableSensor) || (!this.sensorAvalible))
						this.gameView.mRenderer.labyrinth.MakeTurn(1, 0);
					return true;
				case KeyEvent.KEYCODE_DPAD_UP:
					if ((!this.enableSensor) || (!this.sensorAvalible))
						this.gameView.mRenderer.labyrinth.MakeTurn(0, -1);
					return true;
				case KeyEvent.KEYCODE_DPAD_DOWN:
					if ((!this.enableSensor) || (!this.sensorAvalible))
						this.gameView.mRenderer.labyrinth.MakeTurn(0, 1);
					return true;
				case KeyEvent.KEYCODE_BACK:
					this.setState(STATE_MENU, 0);
					this.gameView.mRenderer.mManager.setCurrentLayer(MMManager.LAYER_MAIN);
					return true;
				default:
					return false;
			}	    	
    	}		
    	
    	return false;
    }
    
    private boolean mainMenuKey(View v, int keyCode, KeyEvent event) 
    {
    	if (event.getAction() == KeyEvent.ACTION_DOWN)
		{
			switch (keyCode)
			{
				case KeyEvent.KEYCODE_BACK:
					if (this.gameView.mRenderer.mManager.getCurrentLayer() == MMManager.LAYER_OPTIONS)
					{
						if (this.gameView.mRenderer.mManager.getAnimationEnabled())
							this.mLevelsIOProvider.saveAnimation(this, 1);
						else
							this.mLevelsIOProvider.saveAnimation(this, 0);
						
						if (this.gameView.mRenderer.mManager.getSoundEnabled())
							this.mLevelsIOProvider.saveSound(this, 1);
						else
							this.mLevelsIOProvider.saveSound(this, 0);
						
						if (this.gameView.mRenderer.mManager.getSensorEnabled())
							this.mLevelsIOProvider.saveSensor(this, 1);
						else
							this.mLevelsIOProvider.saveSensor(this, 0);
					}
					Log.d("onKey", Integer.toString(this.gameView.mRenderer.mManager.getCurrentLayer()));
					if (this.gameView.mRenderer.mManager.getCurrentLayer() == MMManager.LAYER_MAIN)
					{
						this.finish();
						System.exit(0);
					}
					else
						this.gameView.mRenderer.mManager.setCurrentLayer(MMManager.LAYER_MAIN);
					Log.d("onKey", Integer.toString(this.gameView.mRenderer.mManager.getCurrentLayer()));
					return true;
				default:
					return false;
			}
		}
		
		return false;
    }
    
	public boolean onKey(View v, int keyCode, KeyEvent event) 
	{					
		if (this.state == GameScreen.STATE_MENU)
			return this.mainMenuKey(v, keyCode, event);
		
		if (this.state == GameScreen.STATE_GAME)
			return this.gameKey(v, keyCode, event);
		
		return false;
	}		
	
	public void onSensorChanged(SensorEvent event) 
	{
		/*if ((!this.enableSensor) || (!this.sensorAvalible))
			return;
		
		synchronized (this) 
		{
			if (event.sensor.getType() == Sensor.TYPE_ORIENTATION)
			{
				if (((event.values[1] - this.oldPitch) > 8.0f) && (event.values[1] > 0.0f))
				{
					this.gameView.mRenderer.labyrinth.MakeTurn(0, -1);
					return;
				}
				
				if (((event.values[1] - this.oldPitch) < -8.0f) && (event.values[1] < 0.0f))
				{
					this.gameView.mRenderer.labyrinth.MakeTurn(0, 1);				
					return;
				}
				
				if (((event.values[2] - this.oldRoll) > 8.0f) && (event.values[2] > 0.0f))
				{
					this.gameView.mRenderer.labyrinth.MakeTurn(1, 0);
					return;
				}
				
				if (((event.values[2] - this.oldRoll) < -8.0f) && (event.values[2] < 0.0f))
				{
					this.gameView.mRenderer.labyrinth.MakeTurn(-1, 0);
					return;
				}
				
				this.oldPitch = event.values[1];
				this.oldRoll = event.values[2];
			}
		}*/
	}

    public void onLongPress(MotionEvent e) 
	{  
    }
	
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
		// TODO Auto-generated method stub
		
	}

	public boolean onDown(MotionEvent arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
			float velocityY) {
		// TODO Auto-generated method stub
		return false;
	}

	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX,
			float distanceY) {
		// TODO Auto-generated method stub
		return false;
	}

	public void onShowPress(MotionEvent e) {
		// TODO Auto-generated method stub
		
	}

	public boolean onSingleTapUp(MotionEvent e) {
		// TODO Auto-generated method stub
		return false;
	}
}
