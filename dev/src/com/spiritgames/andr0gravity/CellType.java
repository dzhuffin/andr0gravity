package com.spiritgames.andr0gravity;

public enum CellType 
{
	Usual(1),			// ������� ������ 
	Finish(2), 			// �����
	Key(4), 			// ���� �� �����
	Unknown(0);			// ����������� ���
	
	private int mask;
	
	CellType(int m)
	{
		this.mask = m;
	}
	
	public int getMask() 
	{
		return this.mask;
	}
	
	public static CellType getByMask(int m)
	{
		switch(m)
		{
			case 1:
				return CellType.Usual;
			case 2:
				return CellType.Finish;
			case 4:
				return CellType.Key;
			default:
				return CellType.Unknown;
		}		
	}
}
